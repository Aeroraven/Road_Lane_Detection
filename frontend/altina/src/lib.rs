use wasm_bindgen::prelude::*;
use js_sys::*;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}

#[wasm_bindgen]
extern "C" {
    fn alert(s: &str);
    #[wasm_bindgen(js_namespace = console)]
    fn log(msg: &str);
    #[wasm_bindgen(js_namespace = console, js_name = log)]
    fn log_usize(a: usize);

}

#[wasm_bindgen]
pub fn greet(name: &str) {
    alert(&format!("Hello, {}!", name));
}

#[wasm_bindgen]
pub fn altina_image_to_tensor_preproc(image: &[u8], out: &mut [f32], image_height: u32, image_width: u32) -> Float32Array{
    let mut i:usize = 0;
    let mut ip: usize = 0;
    let offset:usize = (image_height*image_width) as usize;
    let offset_d:usize = offset * 2;
    log_usize(image.len());
    while i < offset{
        out[i] = (image[ip] as f32) / 58.395 - 2.117903930131004;
        out[i+offset] = (image[ip+1] as f32) / 57.12 - 2.035714285714286;
        out[i+offset_d] = (image[ip+2] as f32) / 57.375 - 1.804444444444444;
        i+=1;
        ip+=4;
    }
    unsafe{
        return Float32Array::view(out);
    }
}