## **Road** Lane Detection

Road Lane Detection

### Collaborators

This repository contains codes form following collaborators:



### Prerequisites 

The basic items

| Purpose/Item | Requirement        |
| ------------ | ------------------ |
| **Python**   | **Version >=3.9**  |
| **Node.js**  | Latest is the best |
| Java         | **Version=8**      |
| Android SDK  | **Version=30**     |
| Rust         | Latest is the best |
| Rust Cargo   | Latest is the best |
| Perl         | Latest is the best |

### Prerequisites (Model & Backend)

**Environment Prerequsites**

Python3.9 is required. And the latest Python version is recommended.

CUDA installation involves CUDA itself and CUDNN libraries.

CUDA, CUDNN and TensorRT path should be in `PATH` environmental variable for Windows.

| Purpose/Item                  | Version Requirement |
| ----------------------------- | ------------------- |
| CUDA (Training)               | >= 10.2             |
| CUDA (CPU Inference)          | >= 10.2             |
| CUDA (CUDA Inference)         | >= 11.0             |
| CUDA (TensorRT Inference)     | >= 11.0             |
| TensorRT (Training)           | Unnecessary         |
| TensorRT (CPU Inference)      | Unnecessary         |
| TensorRT (CUDA Inference)     | Unnecessary         |
| TensorRT (TensorRT Inference) | >=8.2               |
| **Python**                    | **>=3.9**           |

**Runtime Libraries Required：**

Libraries below should be added to the `PATH` environmental variable for Windows. And these can be installed via `apt-get` in Linux.

- libjpeg-turbo (Official Page:https://libjpeg-turbo.org/)

**Python Packages Required:**

Versions of packages are significantly crucial. Mismatched versions will cause the unexpected behaviours of the application. Check package versions via `pip3 list` or `python -m pip list`.

| Package Name                  | Version Requirement    |
| ----------------------------- | ---------------------- |
| OpenCV (opencv-python)        | **>=  4.5.3**          |
| PyTorch (torch)               | **>= 1.8 (>=cu102)**   |
| PyTorch Vision (torchvision)  | **>= 0.9.0 (>=cu102)** |
| ONNXRuntime (onnxruntime-gpu) | <b>\~1.10.0</b>        |
| ONNXRuntime (onnxruntime)     | <b>\~1.10.0</b>        |
| albumentations                | **>= 1.0.0 < 1.1.0**   |
| segmentation-models-pytorch   | >= 0.2.0               |
| torchsummary                  | >= 1.5.0               |
| PyYAML                        | >= 5.4.0               |
| torchmetrics                  | **>= 0.7.3**           |
| tqdm                          | >= 4.62.0              |
| matplotlib                    | >= 3.4.0               |
| jpeg4py                       | >=0.1.0                |
| NumPy                         | Depend on dependencies |
| Pandas                        | >= 1.3.2               |
| scikit-learn                  | >= 0.24.0              |
| scipy                         | >= 1.7.0               |
| flask                         |                        |
| flask-socketio                |                        |

**About Back-End Running:**

- ONNX Modules are not uploaded for this causes the instability of repository cloning operation.
- **PLEASE ENSURE THAT THE MODEL EXISTS BEFORE RUNNING FLASK**

### Prerequisites (Frontend)

- Rust & Cargo

  - Perl is needed during the crate compiling period

- Vue 3 
  
  - Follow the instructions here https://vuejs.org
  
- Cordova & JDK8 & Android Studio & Android SDK 30

  

### How to Start?

**Install Packages**: Check whether prerequisites above are met.

```
pip install <package>
conda install <package>
```

**To Train The Model**：Modify `global_config_server_side.yaml` if you want to train your model at the server side. Otherwise, modify `global_config_client_side.yaml`. Make sure that the paths to the dataset are correct.

Then

```
python train.py
```

You have to select to whether to train the model on your server or not via an input prompt. This dialog window will only appear once.

 **To Deploy The Model**: Just follow the instruction below

```
python deploy.py
```

**To Run Frontend**

```
npm install
npm run dev
```

### Models Supported

- Semantic-Segmentation-Based Algorithms
  - UNet2D
  - SCNN Segmentation Branch
  - YOLOP Lane Seg Branch
- Anchor-Based Algorithms
  - SwiftLane
- Curve-Regression-Based Algorithms
  - SwiftRegressor
  - Modified PolyLane

----

### Acknowledgements

**Code & Resource References**

| Project                                                      | Type                 | License        | Source                          |
| ------------------------------------------------------------ | -------------------- | -------------- | ------------------------------- |
| **SCNN**<br/>XingangPan                                      | Model                | MIT            | GitHub                          |
| **SCNN_Pytorch** <br/>harryhan618                            | Model                | MIT            | GitHub                          |
| **RANSAC Polynomial Regressor**<br/>Jirka                    | Model                | -              | Stackoverflow Question 55682156 |
| **YOLOP** <br/>HUST Visual Learning Team (HUSTVL)            | Model                | MIT            | GitHub                          |
| **Ultra Fast Lane Detection**<br/>cfzd                       | Model                | MIT            | GitHub                          |
| **Novecento Sans** (Normal)<br/>**Novecento Sans** (Wide Bold)<br/>Jan Tonellato | UI/UX - Font         | -              | Adobe Font                      |
| **Source Han Sans / Noto Sans Han**<br/>(Alias: 思源黑体)<br/>Adobe / Google | UI/UX - Font         | SIL Open Font  | GitHub                          |
| **Bender** <br/>Jovanny Lemonad                              | UI/UX - Font         | -              | -                               |
| **Geometos** <br/>Deepak Dogra                               | UI/UX - Font         | Non-commercial | DAFont                          |
| **Source Han Serif <br/> Noto Sans  Han Serif**<br/>(Alias: 思源宋体)<br/>Adobe / Google | UI/UX - Font         | SIL Open Font  | GitHub                          |
| **SVG-Rotation**<br/>ak.hypergryph.com                       | UI/UX - Icon         |                | ak.hypergryph.com               |
| **Proton-Engine-Example**<br/>drawcall                       | Code<br/>UI/UX - Eff | MIT            | GitHub                          |
| **OpenCV.js**<br/>OpenCV                                     | Graphics             | Apache         | OpenCV                          |

**Other Acknowledgements**

| Project                             | Type            | License | Source |
| ----------------------------------- | --------------- | ------- | ------ |
| **Naive UI**<br/>TuSimple Inc.      | Dependency      | MIT     | GitHub |
| **XIcons**<br/>07akioni             | Icon References | -       | GitHub |
| **Socket.IO**                       | Dependency      | -       | GitHub |
| **Axios**                           | Dependency      | -       | GitHub |
| **Arknights**<br/>ak.hypergryph.com | Layout Style    | -       | -      |

**Additional Statements**

License for XIcons coincides with its source repositories including [`fluentui-system-icons`](https://github.com/microsoft/fluentui-system-icons), [`ionicons`](https://github.com/ionic-team/ionicons), [`ant-design-icons`](https://github.com/ant-design/ant-design-icons), [`material-design-icons`](https://github.com/google/material-design-icons), [`Font-Awesome`](https://github.com/FortAwesome/Font-Awesome), [`tabler-icons`](https://github.com/tabler/tabler-icons) and [`carbon`](https://github.com/carbon-design-system/carbon/tree/main/packages/icons).

----
### Notes
There might be some visual elements resembling Game `Arknights`. However, there's no code plagiarism to do with this game's design.

Existing Problems: 

- UnicodeDecodeError: 'cp932' codec can't decode byte 0x84 in position 62: illegal multibyte sequence
  - Delete all **NON-ASCII CHARACTERS** in files. (Characters includes CJKV characters(eg. Chinese), emoji and so on)

----

### Change Logs

**20220506**

- 优化交互
  - 提升实时交互的视觉感受
  - 移除Socket

**20220504**

- 移植Android
  - 优化前端页面

**20220503**

- 整合可视化
  - 整合UltraLane可视化

**20220502**

- 前端调试
  - 整合OpenCV.js
  - 将前端项目从Vite迁移至Webpack5（部分npm包的兼容性问题）
  - 整合ORTWeb

**220501**

- 改良模型 & CULane

  - CULane使用原作者的基本增强和Grid处理

    - 降低其他增强的强度和频率

  - 替换模型车道线分割分支，由UNet变为UltraLane的分割分支

  - Encoder由EfficientNet-b4改为ResNet18

  - 将GroupNorm换为BatchNorm

  - 使用ImageNet预训练参数

  - 混合数据集使用所有数据

    

**220424**

- Refine UI
  - Deprecate portrait viewport
  - Add mocked loading page
  - Improve mobile compatibility

**220422**

- Refine AlwenNetMK2 & CULane:
  - Replace `BatchNorm` with `GroupNorm`
  - Replace backbone `efficientnet-b3` with `efficientnet-b4`
  - Reduce hyper-parameter `h` from 49 to 18
  - Introduce horizontal flip to CULane
- Miscellaneous adjustments
  - Checkpoints now will be saved after validation phases

 

