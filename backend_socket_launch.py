from flask import Flask
from flask_socketio import SocketIO, emit
from backend.image_infer import *
from backend.socket_utils import *
from visualizer.v2.inference import *

app = Flask(__name__)
app.config["SECRET_KEY"] = "secret!"
socketio = SocketIO(app, cors_allowed_origins='*')


@socketio.on("connect")
def handle_connect():
    # print("server has connected")
    emit("server_response", "server has connected")


@socketio.on("video_upload")
def handle_connect_2(message):
    image_path = base64_to_image(message['data'])
    # infer = serv_image_infer_single_alwenmk2(image_path, os.path.abspath(os.path.dirname(__file__)))
    infer = ULInferenceSession().inference(image_path)
    image_ret = img_to_base64(infer)
    emit("server_response", image_ret)


if __name__ == "__main__":
    socketio.run(app, port=5001)

# ServModelCache.start_deploy(os.path.abspath(os.path.dirname(__file__)))
ULInferenceSession().start_session(os.path.abspath(os.path.dirname(__file__)) + r"\static\model\ultra.onnx")
