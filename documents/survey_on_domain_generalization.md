> Summary of the following paper:
>
> Generalizing to Unseen Domains: A Survey on Domain Generalization

#### Domain Generalization

1. **Three Categories**: Data Manipulation, Representation Learning, Learning Strategy
   1. **Data Manipulation**:
      1. Augmentation 
      2. Generation
   2. **Representation Learning**: 
      1. Domain-Invariant Representation Learning: Find invariant features
      2. Feature Disentanglement: Disentangle features into domain-specific or domain-shared parts
   3. **Learning Strategies**:
      1. Ensemble Learning
      2. Meta Learning
2. **Reason of Bad Generalization**: Assumption that train set and test set are both subject to the identical and independent distribution.
3. **Data Augmentation**:
   1. **Typical methods:** flipping, rotation, scaling, cropping, adding noise
   2. **Domain Randomization:** 
      1. Basic Idea: Generate new data based on limited data to simulate the complex situation
      2. Typical Implementations:
         1. Alteration of object attributes (textures or locations)
         2. Alteration of illumination 
         3. Alteration of camera view
         4. Addition of variant noise
      3. Papers: 
         1. Domain Randomization for Scene-Specific Car Detection and Pose Estimation https://arxiv.org/abs/1811.05939
            2. First Pass: Estimate camera parameters, extract background and generate a 3D scene using scene geometries and homography
            2. Generation Pass: Add 3D models to the scene and render an image with annotations
         2. Structured Domain Randomization: Bridging the Reality Gap by Context-Aware Synthetic Data https://arxiv.org/abs/1810.10093
   3. **Adversarial Data Augmentation**:
      1. Basic Idea: Guide augmentation to optimize the generalization capability
      2. Papers:
         1. Learning to Generalize Unseen Domains via Memory-based Multi-Source Meta-Learning
         2. Generalizing to unseen domains via adversarial
         data augmentation.
   4. **Data Generation**:
     1. Typical Implementations:
       1. 

