1. **车道线遮挡严重（例如:CuLane）**
   1. 霍夫变换在CULane上几乎失效，在TuSimple中标线缺失的图片中失效
2. **实时性要求较高**
   1. 基于Anchor的方法预测车道线网格（如：LaneATT、UltraLane、SwiftLane等）
      1. Flatten层导致后续FC出现大量参数(模型过大由FC导致)
      2. Flatten导致位置信息丢失？
         1. SwiftLane的CAM过于混乱
   2. 精简模型（如：SwiftLane 或 蒸馏法）
      1. 一些基于Transformer的模型估计对硬件要求较高
3. **车道线形状相对规则：**
   1. 基于形状先验，类似RNN进行处理CNN（如：SCNN、RESA、2008.03922（ConvGRU））
      1. SCNN效率过低，RESA运行时间为SCNN的24%
   2. 通过损失函数规范化预测结果（如：UltraLane）
      1. 论文中的损失函数合理性待商榷
   3. 将分类问题转化为回归问题（如：PolyLane（三次多项式）、2203.02431（Bezier））
      1. 收敛速度没一般的CNN快
   4. 引入位置信息（如：2203.12301（改进RESA））
4. **道路图像复杂，需要较强泛化能力**
   1. 处理数据集：
      1. 提高泛化能力：增大数据量、数据增强、数据生成、集成学习、Side Information（分割分支、属性分支）、迁移学习等
         1. 数据增强等方法不能改变训练集和实际数据之间的分布差别。SwiftLane的数据增强就对模型提升没什么效果，但将所有样本都放进去后比只放20.jpg效果好。
      2. 减少错误样本：TuSimple存在错误label
      3. 一些常见问题：
         1. 车道线形状扭曲：SwiftLane，RESA
         2. 车道线缺失：UNet，LaneATT
   2. Attention（如：LaneATT）：
      1. Attention层中的Softmax可能会大幅拖慢整体速度
   3. 问题分解（如：YOLOP）
      1. 看起来设备要求很高

5. **模型整理**

| 模型                           | 特点                                                         | 不足                                                         |
| ------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| **SwiftLane<br/>ICMLA 2021**   | 网络模型简单，实时性高                                       | ① 泛化性差 <br/>② CAM显示关注区域混乱 |
| **UltraLane<br/>ECCV 2020**    | 提出基于Anchor的方法，速度快                                 | ① Loss假设不合理<br/> |
| **ViT-PolyLane<br/>WACV 2021** | ① 使用投影得到车道线方程<br/>② 基于Transformer进行回归<br/>③ 简化ResNet18，提高性能<br/>④ 速度为SCNN的60倍 | ① Transformer训练设备要求高<br/>② 内参外参过度近似，推导不合理 |
| PolyLane                       | ① 基于多项式回归<br/>② 使用EfficientNet做Backbone            | ① 弯道拟合性不好<br/> |
| **LaneATT <br/>CVPR 2021**     | ① 注意力机制<br/>② 综合全局特征和局部特征<br/>③ 创新的特征图 | 更容易出现整条车道线缺失                                     |
| YOLOP                          | ① 泛化性好 <br/>② 问题拆分(YOLO)                             | ① 性能过低，速度仅为SCNN的2倍<br/>② 难以训练，无用分类太多   |
| **BezierLane<br/>CVPR 2022**   | ① 特征翻转融合 <br/>② Bezier曲线回归 <br/>③ CAM可解释性强    | 直行车道拟合效果略有偏差                                                            |
| **SCNN**<br/>**AAAI 2018**     | 形状先验，类似RNN的卷积                                      | 空间卷积引入大量卷积层，极度拖慢速度                         |
| RESA-PE                        | ① 优化RESA <br/>② 引入Position Embedding<br/>③ Encoder-Decoder 结构 | 速度为UltraLane的25%                                         |
| **RESA<br/>AAAI 2021**         | ① 提高速度(基于SCNN) <br/>② 类似RNN的处理策略                | 速度为UltraLane的25%                                         |
| **CLRNet <br/>CVPR 2022**      |                                                              |                                                              |

