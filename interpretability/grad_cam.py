import sys

import albumentations
import cv2
import matplotlib.pyplot as plt
import numpy as np
import torch
import tqdm

from model.swiftlane_cnn import SwiftLaneCNNV2
from utils.preprocessing import preset_processing, preset_processing_2


class SwiftLaneGradCAM:
    grad_block = []
    feature_block = []

    @staticmethod
    def backawrd_hook(module, grad_in, grad_out):
        SwiftLaneGradCAM.grad_block.append(grad_out[0].detach())

    @staticmethod
    def forward_hook(module, f_in, f_out):
        SwiftLaneGradCAM.feature_block.append(f_out)

    @staticmethod
    def grad_cam(image, f_map, grad):
        grad = grad.reshape([grad.shape[0], -1])
        cam = np.zeros(f_map.shape[1:], dtype='float')
        weights = np.mean(grad, axis=1)
        for i, w in enumerate(weights):
            cam += w * f_map[i, :, :]
        cam = np.maximum(cam, 0)
        cam = cam / cam.max()
        return cam

    @staticmethod
    def grad_cam_visualize(image, model: SwiftLaneCNNV2, sc, sh, sw):
        model.swift_conv1.register_forward_hook(SwiftLaneGradCAM.forward_hook)
        model.swift_conv1.register_backward_hook(SwiftLaneGradCAM.backawrd_hook)
        output = model(image)
        print("FMAPBL", len(SwiftLaneGradCAM.feature_block))
        f_map = SwiftLaneGradCAM.feature_block[0].detach().cpu().numpy()[0]
        print("FMAP", f_map.shape)


        ret_list = []
        for k in range(sc):
            print("LENRET",len(ret_list))
            p = 0
            cam = np.zeros((1,1))
            with tqdm.tqdm(total= sh, ascii=True, desc="Back-propagating", file=sys.stdout) as t:
                for i in range(sh):

                    t.update(1)
                    idx = np.argmax(output.cpu().data.numpy()[0, k, i])
                    if idx == output.shape[3]-1:
                        continue
                    cl = output[0, k, i, idx]
                    SwiftLaneGradCAM.grad_block = []
                    p += 1
                    cl.backward(retain_graph=True)
                    grad_val = SwiftLaneGradCAM.grad_block[0].cpu().data.numpy().squeeze()
                    if p==0:
                        cam = SwiftLaneGradCAM.grad_cam(image, f_map, grad_val)
                    else:
                        added_cam = SwiftLaneGradCAM.grad_cam(image, f_map, grad_val)
                        cam = cam + added_cam
                        cam /= p
                ret_list.append(cam)
        return ret_list


def minmax_scale(x):
    mmin = np.min(x)
    mmax = np.max(x)
    return (x - mmin) / (mmax - mmin)


if __name__ == "__main__":
    model = torch.load(r"C:\Users\huang\Desktop\Actv\3_670.765964598532.pth").to("cpu")
    model.eval()
    print("Model Loaded")
    image_path = r"E:\MNIST\train_set\clips\0313-1\49080\20.jpg"
    image = cv2.imread(image_path)
    H, W, _ = image.shape
    org_image = image
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    albus = albumentations.Resize(288, 800)
    albus2 = preset_processing_2('imagenet')
    image = albus2(image=image)['image']
    image = albus(image=image)['image']
    image = np.transpose(image, (2, 0, 1))
    image = np.expand_dims(image, 0)
    image = torch.tensor(image)
    output = model(image)
    cam_list = SwiftLaneGradCAM.grad_cam_visualize(image, model, 5, 58, 130)
    for i in range(len(cam_list)):
        cam = minmax_scale(cv2.resize(cam_list[i], (W, H)))
        heatmap = cv2.applyColorMap(np.uint8(255 * cam), cv2.COLORMAP_JET)
        cam_img = np.uint8(0.3 * heatmap + 0.7 * org_image)
        cam_img = cv2.cvtColor(cam_img, cv2.COLOR_BGR2RGB)
        plt.subplot(3, 4, 1 + i * 2)
        plt.title("Weighted Heatmap, Lane " + str(i))
        plt.imshow(minmax_scale(cam_img))
        plt.subplot(3, 4, 2 + i * 2)
        plt.title("Grad Activation, Lane " + str(i))
        plt.imshow(minmax_scale(cam))
    plt.show()
