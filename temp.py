import PIL
from matplotlib import pyplot as plt
import torchsummary

from model.yolop import YOLOPNet
from utils.utility import *
from PIL import Image
from utils.augmentation import *
import cv2
from visualizer.v2.inference_al2 import *
from visualizer.video import VideoVisualizer


def test(**kwargs):
    fargs = {}
    for key in kwargs.keys():
        fargs[key] = kwargs[key]

    # Model Configurations
    preproc = get_preproc_func(**fargs)
    train_dataset = get_train_dataset(preproc, classes_x=[0, 1], **fargs)
    random.seed(time.time())
    w, m = train_dataset[random.randint(0, len(train_dataset))]
    plt.subplot(1, 2, 1)
    plt.imshow(m)
    plt.subplot(1, 2, 2)
    plt.imshow(np.transpose(w, (1, 2, 0)))
    plt.show()


def test2(**kwargs):
    model_container = get_model(mode="test", **kwargs)
    model_param = torch.load(kwargs['model_path'])
    model_container.load_state_dict(model_param)
    # graph = PyTorchConverter().convert(model, dummy_input)
    # print(graph)


def test3():
    # test2(**get_config_json())
    sf = ULCustomCompose([
        ULRandomRotate(6),
        ULRandomUDOffsetLABEL(100),
        ULRandomLROffsetLABEL(200)
    ])
    fw = PIL.Image.open(r"C:\Users\huang\Desktop\ff\05081544_0305\05081544_0305-000001.jpg")
    fw2 = PIL.Image.open(r"E:\Nf\laneseg_label_w16\laneseg_label_w16\driver_23_30frame\05151640_0419.MP4\00030.png")
    print(fw.size, fw2.size)
    fw, fw2 = sf(fw, fw2)
    fw2 = np.array(fw2)
    print(np.max(fw2))
    plt.imshow(fw2)
    plt.show()


if __name__ == "__main__":
    sess = UL2InferenceSession()
    sess.start_session("C:\\Users\\huang\\Desktop\\best.onnx",
                       r"C:\即时通讯文件\1531052461\FileRecv\yolov5s.onnx")
    img = sess.inference(r"C:\WR\5.jpg")
    # img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
    plt.imshow(img)
    plt.show()
