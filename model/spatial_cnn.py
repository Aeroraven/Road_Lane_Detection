# Acknowledgements:

import torch
import torch.nn as nn
import torch.nn.functional as functional
import torchvision.models as tvmodel


class SpatialCNN(torch.nn.Module):
    """
    PyTorch implementation of paper "Spatial As Deep: Spatial CNN for Traffic Scene Understanding"
    This implementation currently contains segmentation branch only

    During the re-implementation procedure, following repositories are referenced.

    .. [1] https://github.com/harryhan618/SCNN_Pytorch/ (MIT License)
    .. [2] https://github.com/XingangPan/SCNN (MIT License)
    """
    def __init__(self,
                 image_shape_w: int,
                 image_shape_h: int,
                 seg_class: int = 2,
                 scnn_feature_w: int = 3,
                 backbone_arch: str = "vgg16",
                 backbone_pretrained: bool = False,
                 mask_shape_w: int = None,
                 mask_shape_h: int = None,
                 final_activation: str = "sigmoid",
                 upsampling_interpolation: str = "bilinear"):
        """
        Initialize the SCNN model

        :param image_shape_w: Image width to be scaled to
        :param image_shape_h: Image height to be scaled to
        :param seg_class: Classes for semantic segmentation
        :param scnn_feature_w: Kernel width for SCNN layers
        :param backbone_arch: Architecture for the backbone network
        :param backbone_pretrained: Load the pretrained parameter for the backbone network
        :param mask_shape_w: Width of output mask
        :param mask_shape_h: Height of output mask
        :param final_activation: Activation function of final layer
        :param upsampling_interpolation: Interpolation method for the final layer
        """
        super(SpatialCNN, self).__init__()
        self.iw, self.ih = image_shape_w, image_shape_h
        self.cl = seg_class
        self.fw = scnn_feature_w
        self.ui = upsampling_interpolation
        if mask_shape_w is None or mask_shape_h is None:
            self.mw, self.mh = self.iw, self.ih
        else:
            self.mw, self.mh = mask_shape_w, mask_shape_h
        # Backbone
        if backbone_arch == "vgg16":
            self.backbone = tvmodel.vgg16_bn(pretrained=backbone_pretrained).features
            # Convolution dilation from [1]
            for i in [34, 37, 40]:
                conv = self.backbone._modules[str(i)]
                dilated_conv = nn.Conv2d(
                    conv.in_channels, conv.out_channels, conv.kernel_size, stride=conv.stride,
                    padding=tuple(p * 2 for p in conv.padding), dilation=2, bias=(conv.bias is not None)
                )
                dilated_conv.load_state_dict(conv.state_dict())
                self.backbone._modules[str(i)] = dilated_conv
            self.backbone._modules.pop('33')
            self.backbone._modules.pop('43')
            self.sw = image_shape_w // 32  # output shape of encoder
            self.of = 512  # output features of encoder
        elif backbone_arch == "vgg11":
            self.backbone = tvmodel.vgg11_bn(pretrained=backbone_pretrained).features
            self.sw = image_shape_w // 32  # output shape of encoder
            self.of = 512  # output features of encoder
        elif backbone_arch == "resnet34":
            self.backbone = tvmodel.resnet18(pretrained=backbone_pretrained)
            self.sw = image_shape_w // 32  # output shape of encoder
            self.of = 512  # output features of encoder
        else:
            self.backbone = None
            raise Exception("Unsupported backbone arch")

        # Top convolution layer
        self.top_conv = torch.nn.Sequential(
            nn.Conv2d(self.of, 1024, 3, padding=4, dilation=4, bias=False),
            nn.BatchNorm2d(1024),
            nn.ReLU(),
            nn.Conv2d(1024, 128, 1, bias=False),
            nn.ReLU()
        )
        # SCNN layers
        self.scnn_u = nn.Conv2d(128, 128, (1, self.fw), padding=(0, self.fw // 2), bias=False)
        self.scnn_d = nn.Conv2d(128, 128, (1, self.fw), padding=(0, self.fw // 2), bias=False)
        self.scnn_l = nn.Conv2d(128, 128, (self.fw, 1), padding=(self.fw // 2, 0), bias=False)
        self.scnn_r = nn.Conv2d(128, 128, (self.fw, 1), padding=(self.fw // 2, 0), bias=False)

        # Semantic segmentation output layer
        self.final_conv = torch.nn.Sequential(
            nn.Dropout2d(0.1),
            nn.Conv2d(128, self.cl, 1)
        )
        # Final activation layer
        if final_activation == "sigmoid":
            self.final_actv = nn.Sigmoid()
        else:
            raise Exception("Final activation is not supported", final_activation)

    def forward(self, x):
        x = self.backbone(x)
        x = self.top_conv(x)
        x = self.scnn_forwarding(x, self.scnn_d, True, False)  # scnn_d
        x = self.scnn_forwarding(x, self.scnn_u, True, True)  # scnn_u
        x = self.scnn_forwarding(x, self.scnn_r, False, False)  # scnn_r
        x = self.scnn_forwarding(x, self.scnn_l, False, True)  # scnn_l
        x = self.final_conv(x)
        x = functional.interpolate(x, (self.mw, self.mh), align_corners=True, mode=self.ui)
        x = self.final_actv(x)
        return x

    def scnn_forwarding(self, x, conv, vertical, rev):
        if vertical:
            sl = [x[:, :, i:(i + 1)] for i in range(x.shape[2])]
            dimension = 2
        else:
            sl = [x[:, :, :, i:(i + 1)] for i in range(x.shape[3])]
            dimension = 3
        if rev:
            sl = sl[::-1]
        out = [sl[0]]
        for i in range(1, len(sl)):
            out.append(sl[i - 1] + functional.relu(conv(out[i - 1])))
        if rev:
            sl = sl[::-1]
        return torch.cat(sl, dim=dimension)
