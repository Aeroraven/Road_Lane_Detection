import torch
import torchmetrics
from torch import nn
import torch.nn.functional as torch_func
import segmentation_models_pytorch as smp


class AL2SoftmaxFocalLoss(nn.Module):
    loss_value = 0.0

    def __init__(self, gamma, ignore_lb=255, *args, **kwargs):
        super(AL2SoftmaxFocalLoss, self).__init__()
        self.gamma = gamma
        self.nll = nn.NLLLoss(ignore_index=ignore_lb)

    def forward(self, logits, labels):
        scores = torch_func.softmax(logits, dim=1)
        factor = torch.pow(1. - scores, self.gamma)
        log_score = torch_func.log_softmax(logits, dim=1)
        log_score = factor * log_score
        loss = self.nll(log_score, labels)
        AL2SoftmaxFocalLoss.loss_value = loss
        return loss


class AL2UltraLaneAccuracy(nn.Module):
    def __init__(self, device="cuda"):
        super(AL2UltraLaneAccuracy, self).__init__()
        self.accm = torchmetrics.Accuracy().to(device)

    def forward(self, pr, gt):
        pr = torch.argmax(pr[0], dim=1)
        return 1 - self.accm(pr, gt[1])


class AL2ArrowDiceLoss(torch.nn.Module):
    meter_value = 4.0
    cl1_dice = 0.0

    def __init__(self, device="cuda"):
        super(AL2ArrowDiceLoss, self).__init__()
        self.celoss = torch.nn.CrossEntropyLoss().to(device)
        self.meter = torchmetrics.JaccardIndex(2).to(device)
        self.dice_fore = smp.losses.DiceLoss("multiclass", from_logits=True, classes=[1])
        self.dice_back = smp.losses.DiceLoss("multiclass", from_logits=True, classes=[0])

    def forward(self, pr, gt):
        AL2ArrowDiceLoss.cl1_dice = self.dice_fore(pr, gt) * 0.9 + self.dice_back(pr, gt) * 0.1
        return AL2ArrowDiceLoss.cl1_dice


class AL2IoUMetric(torch.nn.Module):
    def __init__(self, classes=2, device="cuda"):
        super(AL2IoUMetric, self).__init__()
        self.meter = torchmetrics.JaccardIndex(classes).to(device)

    def forward(self, pr, gt):
        return self.meter(pr[1], gt)


class AL2LsegDiceLoss(torch.nn.Module):
    meter_value = 4.0
    cl1_dice = 0.0

    def __init__(self, device="cuda"):
        super(AL2LsegDiceLoss, self).__init__()
        self.celoss = torch.nn.CrossEntropyLoss().to(device)
        self.meter = torchmetrics.JaccardIndex(2).to(device)
        self.dice = smp.losses.DiceLoss("multiclass", from_logits=True)

    def forward(self, pr, gt):
        AL2LsegDiceLoss.cl1_dice = self.dice(pr, gt)
        return AL2LsegDiceLoss.cl1_dice


class AL2MultitaskLoss(torch.nn.Module):
    task_type = 0

    def __init__(self, device="cuda"):
        super(AL2MultitaskLoss, self).__init__()
        self.lseg_loss = AL2LsegDiceLoss(device)
        self.arrow_loss = AL2ArrowDiceLoss(device)
        self.lane_loss = AL2SoftmaxFocalLoss(2,device=device)

    def forward(self, y_pr, y_gt):
        # PR (anc, arr, lsg)
        # GT-CULane (mask, anchor)
        # GT-Apollo mask
        if type(y_gt) is tuple or type(y_gt) is list:
            AL2MultitaskLoss.task_type = 0
            loss = self.lane_loss(y_pr[0], y_gt[1]) * 1
            loss += self.lseg_loss(y_pr[2], y_gt[0]) * 1
        else:
            AL2MultitaskLoss.task_type = 1
            loss = self.arrow_loss(y_pr[1], y_gt) * 0.5
        return loss

    @staticmethod
    def do_lane_update(*args):
        return AL2MultitaskLoss.task_type == 0

    @staticmethod
    def do_arrow_update(*args):
        return AL2MultitaskLoss.task_type == 1


class AL2LsegLossMetric(nn.Module):
    def __init__(self):
        super(AL2LsegLossMetric, self).__init__()

    def forward(self, *args):
        return AL2LsegDiceLoss.cl1_dice


class AL2ArrowLossMetric(nn.Module):
    def __init__(self):
        super(AL2ArrowLossMetric, self).__init__()

    def forward(self, *args):
        return AL2ArrowDiceLoss.cl1_dice


class AL2AnchorLossMetric(nn.Module):
    def __init__(self):
        super(AL2AnchorLossMetric, self).__init__()

    def forward(self, *args):
        return AL2SoftmaxFocalLoss.loss_value