import numpy as np
import torch
import torch.nn.functional as torch_f
from segmentation_models_pytorch.utils import base


class PolyLaneExistCE(base.Loss):
    def __init__(self, c, deg=3, start_y=160):
        super(PolyLaneExistCE, self).__init__()
        self.c = c
        self.d = deg
        self.s = start_y
        self.f_ce = torch.nn.CrossEntropyLoss()

    def forward(self, y_pr, y_gt):
        # Y_PR: (BN,(x_pr(2*c),x_re(k*c),x_st(1*c),x_vh(1))
        # Y_GT: Coord(BN,C,H), Exist(BN,C), Top(BN,C), Bottom(BN,C)
        y_gt_e = y_gt[1]
        t_pr = torch.reshape(y_pr[:, :2 * self.c], (y_pr.shape[0], self.c, 2))
        t_pr = torch_f.softmax(t_pr, dim=-1)
        t_pr = torch.transpose(t_pr,2,1)
        loss_ce = self.f_ce(t_pr, y_gt_e)
        return loss_ce


class PolyLaneExistenceLaneErrorLoss(base.Loss):
    def __init__(self, c, deg=4, start_y=160, device="cuda", w1=1, w2=1):
        super(PolyLaneExistenceLaneErrorLoss, self).__init__()
        self.c = c
        self.d = deg
        self.s = start_y
        self.dev = device
        self.w1 = w1
        self.w2 = w2
        self.f_ce = torch.nn.CrossEntropyLoss()

    def forward(self, y_pr, y_gt):
        # Y_PR: (BN,(x_pr(2*c),x_re(k*c),x_st(1*c),x_vh(1))
        # Y_GT: Coord(BN,C,H), Exist(BN,C), Top(BN,C), Bottom(BN,C)
        y_gt_c = y_gt[0]
        y_gt_e = y_gt[1]
        y_gt_t = y_gt[2]
        y_gt_b = y_gt[3]

        t_pr = torch.reshape(y_pr[:, :2 * self.c], (y_pr.shape[0], self.c, 2))
        t_pr = torch_f.softmax(t_pr, dim=-1)
        t_pr = torch.transpose(t_pr,2,1)
        t_z = (np.array([i * 10 + self.s for i in range(y_gt_c.shape[2])]))
        t_zp = torch.tensor(np.array([t_z ** i for i in range(self.d)]))  # (k,h)
        t_zp = torch.transpose(t_zp, 1, 0).float().to(self.dev)  # x=(H,degs)

        loss_deviation = 0
        loss_ce = self.f_ce(t_pr, y_gt_e)
        loss_s = 0

        for b in range(y_pr.shape[0]):
            h = 0
            for i in range(len(y_gt_t[b])):
                if y_gt_t[b,i]<1e8:
                    h = max(y_gt_t[b,i],h)
            hi = int(int(h - self.s) // 10)
            if h == 0 or hi < 0:
                raise Exception("Invalid H")
            st_l = torch.div(y_gt_b[b]-self.s,10,rounding_mode='floor')
            st_l = st_l.detach().cpu().numpy().astype("int")
            gt_xsp = 0
            for ch in range(y_gt_c.shape[1]):
                if y_gt_e[b, ch] == 0:
                    continue
                coef = torch.reshape(y_pr[b, 2 * self.c + self.d * ch:2 * self.c + self.d * (ch + 1)],(self.d, 1))  # (k)
                pr_x = torch.mm(t_zp, coef)[hi:st_l[ch]]
                gt_x = y_gt_c[b, ch, hi:st_l[ch]]
                gt_xsp = max(gt_xsp, gt_x.shape[0])
                loss_deviation += torch.sum(torch.abs(pr_x - gt_x))
                loss_s += torch.abs(st_l[ch] - y_pr[b, (2 + self.d) * self.c + ch])
                loss_s += torch.abs(hi - y_pr[b, (3 + self.d) * self.c])

        return (self.w1 * loss_s / y_gt_c.shape[1] + loss_deviation / y_gt_c.shape[1] /
                y_gt_c.shape[2]) / y_gt_c.shape[0] + self.w2 * loss_ce
