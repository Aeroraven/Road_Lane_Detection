import numpy as np
import torch
import torchmetrics
from torch import nn
import torch.nn.functional as torch_func


class AlwenIoULoss(nn.Module):
    value = 0.0

    def __init__(self, device="cuda") -> None:
        super().__init__()
        self.iou = torchmetrics.JaccardIndex(2).to(device)

    def forward(self, pred, target):
        AlwenIoULoss.value = self.iou(pred, target)
        return AlwenIoULoss.value

    def to(self, device):
        return self.iou.to(device)


class AlwenMaskSegLoss(nn.Module):
    def __init__(self, device="cuda"):
        super(AlwenMaskSegLoss, self).__init__()
        # self.bce_seg = nn.BCEWithLogitsLoss(pos_weight=torch.Tensor([0.2]).to(device))
        self.ce_seg = nn.CrossEntropyLoss().to(device)
        self.iou = AlwenIoULoss(device)
        self.device = device

    def forward(self, pr, gt):
        lane_line_seg_predicts = pr
        lane_line_seg_targets = gt
        lseg_ll = self.ce_seg(lane_line_seg_predicts, lane_line_seg_targets)
        lane_line_gt = gt
        liou_ll = 1 - self.iou(pr, lane_line_gt)
        return lseg_ll + liou_ll


class MixedLaneCELoss(nn.Module):
    accuracy = 0.0

    def __init__(self, c, h, w, w_mod, focal_gamma=1, device="cuda"):
        super(MixedLaneCELoss, self).__init__()
        self.c = c
        self.h = h
        self.w = w
        self.wm = w_mod
        self.g = focal_gamma
        self.device = device
        self.celoss = nn.CrossEntropyLoss().to(device)
        self.acc_meter = torchmetrics.Accuracy(num_classes=2).to(device)

    def forward(self, y_pr: torch.Tensor, y_gt: torch.Tensor):
        y_gt_idx = y_gt
        y_gt_idx[y_gt < 0] = self.w * self.wm
        y_gt_idx = (y_gt_idx / self.wm).type(torch.int64).to(self.device)
        y_gt_emb = torch_func.one_hot(y_gt_idx, num_classes=self.w + 1)
        y_pr_un = torch.unsqueeze(y_pr, 1)
        y_pr_combined = torch.cat([1 - y_pr_un, y_pr_un], dim=1)
        MixedLaneCELoss.accuracy = self.acc_meter(y_pr_combined, y_gt_emb)
        return self.celoss(y_pr_combined, y_gt_emb)


class MixedLaneConvexConstraintLoss(nn.Module):
    r"""
    This loss aims to constrain the convexity of a lane
    """

    def __init__(self, c, h, w, w_mod, device="cuda"):
        super(MixedLaneConvexConstraintLoss, self).__init__()
        self.c = c
        self.h = h
        self.w = w
        self.wm = w_mod
        self.embedding = torch.Tensor(np.arange(self.w)).float().to(device).view(1, 1, 1, -1)

    def forward(self, y_pr: torch.Tensor, y_gt: torch.Tensor):
        x = y_pr[:, :, :, :self.w]
        pos = torch.sum(x * self.embedding, dim=-1)  # (BN,C,H)
        pos_lw = pos[:, :, :self.h - 2]
        pos_mw = pos[:, :, 1:self.h - 1]
        pos_hw = pos[:, :, 2:self.h]
        neg_constraint_src = torch.sum(torch_func.relu(pos_lw + pos_hw - 2 * pos_mw), -1)
        pos_constraint_src = torch.sum(torch_func.relu(2 * pos_mw - pos_lw - pos_hw), -1)
        min_constraint = torch.minimum(neg_constraint_src, pos_constraint_src)
        batch_sum = torch.sum(min_constraint, dim=-1)
        total_sum = torch.sum(batch_sum)
        return total_sum / y_gt.shape[0] / y_gt.shape[1] / y_gt.shape[2]


class MixedLaneNearFieldConstraintLoss(nn.Module):
    r"""
    This loss aims to constrain the convexity of a lane
    """

    def __init__(self, c, h, w, w_mod, device="cuda"):
        super(MixedLaneNearFieldConstraintLoss, self).__init__()
        self.c = c
        self.h = h
        self.w = w
        self.wm = w_mod
        self.embedding = torch.Tensor(np.arange(self.w)).float().to(device).view(1, 1, 1, -1)
        self.l1 = torch.nn.L1Loss()

    def forward(self, y_pr: torch.Tensor, y_gt: torch.Tensor):
        x = y_pr[:, :, :, :self.w]
        pos = torch.sum(x * self.embedding, dim=-1)  # (BN,C,H)
        diff_list1 = []
        for i in range(0, self.h // 2):
            diff_list1.append(pos[:, :, i] - pos[:, :, i + 1])
        loss = 0
        for i in range(len(diff_list1) - 1):
            loss += self.l1(diff_list1[i], diff_list1[i + 1])
        loss /= len(diff_list1) - 1
        return loss


class AlwenLaneShapeLoss(nn.Module):
    def __init__(self, c, h, w, w_mod, device="cuda"):
        super(AlwenLaneShapeLoss, self).__init__()
        self.loss_curve = MixedLaneNearFieldConstraintLoss(c, h, w, w_mod, device).to(device)
        self.loss_lane = MixedLaneConvexConstraintLoss(c, h, w, w_mod, device).to(device)

    def forward(self, x, _):
        x1 = self.loss_curve(x, _)
        x2 = self.loss_lane(x, _)
        return x1 + x2


class AlwenLaneLoss(nn.Module):
    def __init__(self, c, h, w, w_mod, device="cuda"):
        super(AlwenLaneLoss, self).__init__()
        self.l_shape = AlwenLaneShapeLoss(c, h, w, w_mod, device).to(device)
        self.l_ce = MixedLaneCELoss(c, h, w, w_mod, 1, device).to(device)

    def forward(self, y_pr: torch.Tensor, y_gt: torch.Tensor):
        x1 = self.l_shape(y_pr, y_gt)
        x2 = self.l_ce(y_pr, y_gt)
        return (0.5*x1 + x2) * 10.0 


class AlwenMultiTaskLoss(nn.Module):
    lane_loss_w = 0.0
    arrow_loss_w = 0.0
    task_type = 0

    def __init__(self, c, h, w, w_mod, device="cuda"):
        super(AlwenMultiTaskLoss, self).__init__()
        self.lane_loss = AlwenLaneLoss(c, h, w, w_mod, device)
        self.arrow_loss = AlwenMaskSegLoss(device)

    def forward(self, y_pr: torch.Tensor, y_gt: torch.Tensor):
        if y_gt.shape[1] < 10:
            AlwenMultiTaskLoss.task_type = 0
            AlwenMultiTaskLoss.lane_loss_w = self.lane_loss(y_pr[0], y_gt)
            return AlwenMultiTaskLoss.lane_loss_w
        else:
            AlwenMultiTaskLoss.task_type = 1
            AlwenMultiTaskLoss.arrow_loss_w = self.arrow_loss(y_pr[1], y_gt)
            return AlwenMultiTaskLoss.arrow_loss_w * 1.0

    @staticmethod
    def do_lane_update(*args):
        return AlwenMultiTaskLoss.task_type == 0

    @staticmethod
    def do_arrow_update(*args):
        return AlwenMultiTaskLoss.task_type == 1


# Metrics below are not suitable for distributed learning
# These functions are designed for performance considerations and redundancy considerations

class AlwenLaneLossMetric(nn.Module):
    def __init__(self):
        super(AlwenLaneLossMetric, self).__init__()

    def forward(self, *args):
        return AlwenMultiTaskLoss.lane_loss_w


class AlwenArrowLossMetric(nn.Module):
    def __init__(self):
        super(AlwenArrowLossMetric, self).__init__()

    def forward(self, *args):
        return AlwenMultiTaskLoss.arrow_loss_w


class AlwenArrowIoUMetric(nn.Module):
    def __init__(self):
        super(AlwenArrowIoUMetric, self).__init__()

    def forward(self, *args):
        return AlwenIoULoss.value


class MixedLaneAccuracyMetric(nn.Module):
    def __init__(self):
        super(MixedLaneAccuracyMetric, self).__init__()

    def forward(self, *args):
        return 1-MixedLaneCELoss.accuracy
