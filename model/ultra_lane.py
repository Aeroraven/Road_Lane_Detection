import torch
import torch.nn as nn
import torchsummary

from model.blocks.swift_resnet import ArvnResNetHelper


class UltraFastLane(torch.nn.Module):
    """
    PyTorch implementation of paper "Ultra Fast Structure-aware Deep Lane Detection "
    This is the re-implemented version and not guaranteed to be completely correct.

    """

    def __init__(self,
                 channels: int,
                 height: int,
                 width: int,
                 c: int,
                 h: int,
                 w: int,
                 backbone="resnet50"):
        super(UltraFastLane, self).__init__()
        self.channels = channels
        self.height = height
        self.width = width
        self.ch = c
        self.hl = h
        self.wd = w

        self.backbone = ArvnResNetHelper.get_arch(backbone, channels)
        if backbone in ["resnet34", "resnet18"]:
            self.pool = nn.Conv2d(512, 8, 1)
        else:
            self.pool = nn.Conv2d(2048, 8, 1)
        self.flatten = nn.Flatten()

        wp = torch.ones((1, channels, height, width))
        self.feature_shape = self.feat_forward(wp).shape
        self.cls1 = nn.Linear(self.feature_shape[1], 2048)
        self.clsr = nn.ReLU()
        self.cls2 = nn.Linear(2048, c * h * (w + 1))

        # Init Param
        self.recursive_weight_init(self.cls1)
        self.recursive_weight_init(self.cls2)

    def forward(self, x):
        x = self.feat_forward(x)
        x = self.cls1(x)
        x = self.clsr(x)
        x = self.cls2(x)
        x = torch.reshape(x, (x.shape[0], self.wd + 1, self.ch, self.hl))
        return x

    def feat_forward(self, x):
        x = self.backbone(x)
        x = self.pool(x)
        x = self.flatten(x)
        return x

    def recursive_weight_init(self, m):
        if isinstance(m, list):
            for mini_m in m:
                self.recursive_weight_init(mini_m)
        else:
            if isinstance(m, torch.nn.Conv2d):
                torch.nn.init.kaiming_normal_(m.weight, nonlinearity='relu')
                if m.bias is not None:
                    torch.nn.init.constant_(m.bias, 0)
            elif isinstance(m, torch.nn.Linear):
                m.weight.data.normal_(0.0, std=0.01)
            elif isinstance(m, torch.nn.BatchNorm2d):
                torch.nn.init.constant_(m.weight, 1)
                torch.nn.init.constant_(m.bias, 0)
            elif isinstance(m, torch.nn.Module):
                for mini_m in m.children():
                    self.recursive_weight_init(mini_m)
            else:
                raise Exception('UltraLane: unkonwn module', m)


if __name__ == "__main__":
    model = UltraFastLane(3, 480, 800, 4, 40, 40).to("cpu")
    torchsummary.summary(model,(3,480,800),device="cpu")