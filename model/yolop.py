from torch.nn import *

from model.blocks.yolo_blocks import *


class YOLOConv(nn.Module):
    def __init__(self, in_c, out_c, k, s, csp_n):
        super(YOLOConv, self).__init__()
        self.conv = Conv(in_c, out_c, k, s)
        self.csp = BottleneckCSP(out_c, out_c, csp_n)

    def forward(self, x):
        return self.csp(self.conv(x))


class YOLOConvSPP(nn.Module):
    def __init__(self, in_c, out_c, k, s, csp_n, spp_pm):
        super(YOLOConvSPP, self).__init__()
        self.conv = Conv(in_c, out_c, 3, 2)
        self.spp = SPP(out_c, out_c, spp_pm)
        self.csp = BottleneckCSP(out_c, out_c, csp_n, False)

    def forward(self, x):
        return self.csp(self.spp(self.conv(x)))


class YOLOUpConv(nn.Module):
    def __init__(self, in_c, out_c, out_c2):
        super(YOLOUpConv, self).__init__()
        self.conv = Conv(out_c, out_c2, 1, 1)
        self.ups = Upsample(None, 2, 'nearest')
        self.csp = BottleneckCSP(in_c, out_c, 1, False)

    def forward(self, x):
        return self.ups(self.conv(self.csp(x)))


class YOLOUpConv2(nn.Module):
    def __init__(self, in_c, out_c):
        super(YOLOUpConv2, self).__init__()
        self.conv = Conv(in_c, out_c, 3, 1)
        self.ups = Upsample(None, 2, 'nearest')

    def forward(self, x):
        return self.ups(self.conv(x))


class YOLOPNet(nn.Module):
    """
    PyTorch implementation of paper "You Only Look Once for Panopitic Driving Perception."
    Code reference: https://github.com/hustvl/YOLOP
    Code is refactored for the friendliness consideration

    """

    def __init__(self):
        super(YOLOPNet, self).__init__()
        self.focus = Focus(3, 32, 3)
        self.conv1 = YOLOConv(32, 64, 3, 2, 1)
        self.conv2 = YOLOConv(64, 128, 3, 2, 3)
        self.conv3 = YOLOConv(128, 256, 3, 2, 3)
        self.conv4 = YOLOConvSPP(256, 512, 3, 2, 1, [5, 9, 13])
        self.upconv1 = YOLOUpConv(512, 512, 256)
        self.upconv2 = YOLOUpConv(512, 256, 128)  # Encoder
        self.upconv3 = YOLOUpConv2(256, 128)
        self.upconv4 = YOLOUpConv(128, 64, 32)
        self.upconv5 = Conv(32, 16, 3, 1)
        self.upconv6a = BottleneckCSP(16, 8, 1, False)
        self.upconv6b = Upsample(None, 2, 'nearest')
        self.upconv6c = Conv(8, 2, 3, 1)
        self.actv = Sigmoid()

    def forward(self, x):
        x = self.focus(x)
        x = self.conv1(x)
        x1 = self.conv2(x)
        x2 = self.conv3(x1)
        x3 = self.conv4(x2)
        x3 = self.upconv1(x3)
        x2 = torch.cat([x3, x2], 1)
        x2 = self.upconv2(x2)
        x = torch.cat([x2, x1], 1)
        x = self.upconv3(x)
        x = self.upconv4(x)
        x = self.upconv5(x)
        x = self.upconv6a(x)
        x = self.upconv6b(x)
        x = self.upconv6c(x)
        x = self.actv(x)
        return x


class YOLOPNetReduced(nn.Module):
    """
    PyTorch implementation of paper "You Only Look Once for Panopitic Driving Perception."
    Code reference: https://github.com/hustvl/YOLOP
    Code is refactored for the friendliness consideration

    """

    def __init__(self):
        super(YOLOPNetReduced, self).__init__()
        self.focus = Focus(3, 8, 3)
        self.conv1 = YOLOConv(8, 16, 3, 2, 1)
        self.conv2 = YOLOConv(16, 32, 3, 2, 3)
        self.conv3 = YOLOConv(32, 64, 3, 2, 3)
        self.conv4 = YOLOConvSPP(64, 128, 3, 2, 1, [5, 9, 13])
        self.upconv1 = YOLOUpConv(128, 128, 64)
        self.upconv2 = YOLOUpConv(128, 64, 32)  # Encoder
        self.upconv3 = YOLOUpConv2(64, 32)
        self.upconv4 = YOLOUpConv(32, 16, 8)
        self.upconv5 = Conv(8, 4, 3, 1)
        self.upconv6a = BottleneckCSP(4, 2, 1, False)
        self.upconv6b = Upsample(None, 2, 'nearest')
        self.upconv6c = Conv(2, 2, 3, 1)
        self.actv = Sigmoid()

    def forward(self, x):
        x = self.focus(x)
        x = self.conv1(x)
        x1 = self.conv2(x)
        x2 = self.conv3(x1)
        x3 = self.conv4(x2)
        x3 = self.upconv1(x3)
        x2 = torch.cat([x3, x2], 1)
        x2 = self.upconv2(x2)
        x = torch.cat([x2, x1], 1)
        x = self.upconv3(x)
        x = self.upconv4(x)
        x = self.upconv5(x)
        x = self.upconv6a(x)
        x = self.upconv6b(x)
        x = self.upconv6c(x)
        x = self.actv(x)
        return x


class YOLOPNetEncoder(nn.Module):
    """
    PyTorch implementation of paper "You Only Look Once for Panopitic Driving Perception."
    Code reference: https://github.com/hustvl/YOLOP
    Code is refactored for the friendliness consideration

    """

    def __init__(self):
        super(YOLOPNetEncoder, self).__init__()
        self.focus = Focus(3, 32, 3)
        self.conv1 = YOLOConv(32, 64, 3, 2, 1)
        self.conv2 = YOLOConv(64, 128, 3, 2, 3)
        self.conv3 = YOLOConv(128, 256, 3, 2, 3)
        self.conv4 = YOLOConvSPP(256, 512, 3, 2, 1, [5, 9, 13])
        self.upconv1 = YOLOUpConv(512, 512, 256)
        self.upconv2 = YOLOUpConv(512, 256, 128)  # Encoder


    def forward(self, x):
        x = self.focus(x)
        x = self.conv1(x)
        x1 = self.conv2(x)
        x2 = self.conv3(x1)
        x3 = self.conv4(x2)
        x3 = self.upconv1(x3)
        x2 = torch.cat([x3, x2], 1)
        x2 = self.upconv2(x2)
        x = torch.cat([x2, x1], 1)
        return x