import numpy as np
import torchsummary
from torch import nn

from model.yolop import *


class AlwenNetYOLOPEncoder(nn.Module):
    def __init__(self):
        super(AlwenNetYOLOPEncoder, self).__init__()
        self.focus = Focus(3, 32, 3)
        self.conv1 = YOLOConv(32, 64, 3, 2, 1)
        self.conv2 = YOLOConv(64, 128, 3, 2, 3)
        self.conv3 = YOLOConv(128, 256, 3, 2, 3)
        self.conv4 = YOLOConvSPP(256, 512, 3, 2, 1, [5, 9, 13])
        self.upconv1 = YOLOUpConv(512, 512, 256)
        self.upconv2 = YOLOUpConv(512, 256, 128)  # Encoder


    def forward(self, x):
        x = self.focus(x)
        x = self.conv1(x)
        x1 = self.conv2(x)
        x2 = self.conv3(x1)
        x3 = self.conv4(x2)
        x3 = self.upconv1(x3)
        x2 = torch.cat([x3, x2], 1)
        x2 = self.upconv2(x2)
        x = torch.cat([x2, x1], 1)
        return x

class AlwenNetArrowDecoder(nn.Module):
    def __init__(self):
        super(AlwenNetArrowDecoder, self).__init__()
        self.upconv3pre = Conv(256,256,1,1)
        self.upconv3 = YOLOUpConv2(256, 128)
        self.upconv4 = YOLOUpConv(128, 64, 32)
        self.upconv5 = Conv(32, 16, 3, 1)
        self.upconv6a = BottleneckCSP(16, 8, 1, False)
        self.upconv6b = Upsample(None, 2, 'nearest')
        self.upconv6c = Conv(8, 2, 3, 1)
        self.actv = Sigmoid()

    def forward(self, x):
        x = self.upconv3pre(x)
        x = self.upconv3(x)
        x = self.upconv4(x)
        x = self.upconv5(x)
        x = self.upconv6a(x)
        x = self.upconv6b(x)
        x = self.upconv6c(x)
        x = self.actv(x)
        return x


class AlwenNetLaneDecoder(nn.Module):
    def __init__(self,
                 input_shape,
                 c,
                 h,
                 w):
        super(AlwenNetLaneDecoder, self).__init__()
        self.c = c
        self.h = h
        self.w = w
        self.input_shape = input_shape
        self.swift_maxpool1 = nn.MaxPool2d(2)
        self.swift_conv1 = nn.Conv2d(256, 512, (3, 3))
        self.swift_conv2 = nn.Conv2d(512, 1024, (3, 3))
        self.swift_conv3 = nn.Conv2d(1024, 2048, (3, 3))
        self.swift_maxpool2 = nn.MaxPool2d(2)
        self.swift_fl = nn.AdaptiveAvgPool2d(1)

        self.swift_fl2 = nn.Flatten()
        self.flsp = self.pref_forward(torch.tensor(np.ones(
            (1, input_shape[0], input_shape[1], input_shape[2]))).float()).shape[1]

        self.d1 = nn.Dropout(0.2)
        self.f1 = nn.Linear(self.flsp, 1024)
        self.d2 = nn.Dropout(0.2)
        self.f2 = nn.Identity()
        self.d3 = nn.Identity()
        self.f3 = nn.Linear(1024, c * h * w)
        self.actv = nn.Softmax(-1)

    def pref_forward(self, x):
        x = self.swift_maxpool1(x)
        x = self.swift_conv1(x)
        x = self.swift_conv2(x)
        x = self.swift_conv3(x)
        x = self.swift_fl(x)
        x = self.swift_fl2(x)
        return x

    def forward(self, x):
        x = self.pref_forward(x)
        x = self.d1(x)
        x = self.f1(x)
        x = self.d2(x)
        x = self.f2(x)
        x = self.d3(x)
        x = self.f3(x)
        x = torch.reshape(x, [x.shape[0], self.c, self.h, self.w])
        x = self.actv(x)
        return x


class AlwenNet(nn.Module):
    def __init__(self,
                 image_height: int = 480,
                 image_width: int = 800,
                 c: int = 4,
                 h: int = 58,
                 w: int = 100):
        super(AlwenNet, self).__init__()
        # Parameters
        self.ih = image_height
        self.iw = image_width

        # Blocks
        self.encoder = AlwenNetYOLOPEncoder()
        self.op = self.get_encoder_output()[1:]
        self.lane_branch = AlwenNetLaneDecoder(self.op, c, h, w+1)
        self.arrow_branch = AlwenNetArrowDecoder()

    def get_encoder_output(self):
        x = torch.tensor(np.ones((1, 3, self.ih, self.iw))).float()
        return self.encoder(x).shape

    def forward(self, x):
        x = self.encoder(x)
        x1 = self.lane_branch(x)
        x2 = self.arrow_branch(x)
        return x1, x2

    def summary(self):
        pass


if __name__ == "__main__":
    model = AlwenNet()
    torchsummary.summary(model, (3, model.ih, model.iw), -1, device="cpu")
