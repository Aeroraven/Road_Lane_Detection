import torch
import torch.nn as nn

from model.blocks.swift_resnet import SwiftLaneResBlock


class SwiftLaneCNN(torch.nn.Module):
    """
    PyTorch implementation of paper "SwiftLane: Towards Fast and Efficient Lane Detection"
    The author has not published any codes yet. Thus the correctness cannot be guaranteed.
    This implementation is deprecated for the incorrect implementation of ResNet
    """

    @DeprecationWarning
    def __init__(self,
                 channels: int,
                 height: int,
                 width: int,
                 c: int,
                 h: int,
                 w: int):
        super(SwiftLaneCNN, self).__init__()
        self.channels = channels
        self.height = height
        self.width = width
        self.ch = c
        self.hl = h
        self.wd = w

        # Residual CNN Blocks
        # Initial Block
        self.resnet_conv1 = nn.Conv2d(channels, 64, (7, 7), (2, 2), 3)
        self.resnet_bn1 = nn.BatchNorm2d(64)
        self.resnet_relu1 = nn.ReLU()
        self.resnet_maxpool1 = nn.MaxPool2d(3, 2, 1)

        # First Res Block
        self.resnet_conv2 = nn.Conv2d(64, 64, (3, 3), (1, 1), 1)
        self.resnet_conv2b = nn.Conv2d(64, 64, (3, 3), (1, 1), 1)
        self.resnet_conv3 = nn.Conv2d(64, 64, (3, 3), (1, 1), 1)
        self.resnet_conv3b = nn.Conv2d(64, 64, (3, 3), (1, 1), 1)

        # Second Res Block
        self.resnet_conv4 = nn.Conv2d(64, 128, (3, 3), (2, 2), 1)
        self.resnet_conv4b = nn.Conv2d(128, 128, (3, 3), (1, 1), 1)
        self.resnet_conv4u = nn.Conv2d(64, 128, (1, 1), (2, 2), 0)
        self.resnet_conv5 = nn.Conv2d(128, 128, (3, 3), (1, 1), 1)
        self.resnet_conv5b = nn.Conv2d(128, 128, (3, 3), (1, 1), 1)

        # Third Res Block
        self.resnet_conv6 = nn.Conv2d(128, 256, (3, 3), (2, 2), 1)
        self.resnet_conv6b = nn.Conv2d(256, 256, (3, 3), (1, 1), 1)
        self.resnet_conv6u = nn.Conv2d(128, 256, (1, 1), (2, 2), 0)
        self.resnet_conv7 = nn.Conv2d(256, 256, (3, 3), (1, 1), 1)
        self.resnet_conv7b = nn.Conv2d(256, 256, (3, 3), (1, 1), 1)

        # SwiftLane: Pooling & Conv
        self.swift_maxpool1 = nn.MaxPool2d(2)
        self.swift_conv1 = nn.Conv2d(256, 8, (1, 1))

        # Estimator
        wp = torch.ones((1, 3, height, width))
        self.feature_shape = self.conv_forwarding(wp).shape

        # SwiftLane: Fully Connected
        self.swift_fl = nn.Flatten()
        self.swift_drop1 = nn.Dropout(0.2)
        self.swift_fc2 = nn.Linear(self.feature_shape[1] * self.feature_shape[2] * self.feature_shape[3], 2048)
        self.swift_drop2 = nn.Dropout(0.2)
        self.swift_out = nn.Linear(2048, c * h * (w + 1))

        # Activation
        self.swift_actv = nn.Softmax(1)

    def conv_forwarding(self, x: torch.Tensor):
        # ResNet: Initial Conv
        x = self.resnet_conv1(x)
        x = self.resnet_bn1(x)
        x = self.resnet_maxpool1(x)
        # ResNet: Block 1
        xp = self.resnet_conv2(x)
        xp = self.resnet_conv2b(xp)
        x = x + xp
        xp = self.resnet_conv3(x)
        xp = self.resnet_conv3b(xp)
        x = x + xp
        # ResNet: Block 2
        xp = self.resnet_conv4(x)
        xp = self.resnet_conv4b(xp)
        x = self.resnet_conv4u(x)
        x = x + xp
        xp = self.resnet_conv5(x)
        xp = self.resnet_conv5b(xp)
        x = x + xp
        # ResNet: Block 3
        xp = self.resnet_conv6(x)
        xp = self.resnet_conv6b(xp)
        x = self.resnet_conv6u(x)
        x = x + xp
        xp = self.resnet_conv7(x)
        xp = self.resnet_conv7b(xp)
        x = x + xp
        # SwiftLane: Conv
        x = self.swift_maxpool1(x)
        x = self.swift_conv1(x)
        return x

    def forward(self, x: torch.Tensor):
        x = self.conv_forwarding(x)
        x = self.swift_fl(x)
        x = self.swift_drop1(x)
        x = self.swift_fc2(x)
        x = self.swift_drop2(x)
        x = self.swift_out(x)
        x = self.swift_actv(x)
        x = torch.reshape(x, (x.shape[0], self.ch, self.hl, self.wd + 1))
        return x


class SwiftLaneCNNV2(torch.nn.Module):
    """
    PyTorch implementation of paper "SwiftLane: Towards Fast and Efficient Lane Detection"
    The author has not published any codes yet. Thus the correctness cannot be guaranteed.

    """

    def __init__(self,
                 channels: int,
                 height: int,
                 width: int,
                 c: int,
                 h: int,
                 w: int):
        super(SwiftLaneCNNV2, self).__init__()
        self.channels = channels
        self.height = height
        self.width = width
        self.ch = c
        self.hl = h
        self.wd = w

        # Residual CNN Blocks
        # Initial Block
        self.resnet_conv1 = nn.Conv2d(channels, 64, (7, 7), (2, 2), 3)
        self.resnet_bn1 = nn.BatchNorm2d(64)
        self.resnet_relu1 = nn.ReLU()
        self.resnet_maxpool1 = nn.MaxPool2d(3, 2, 1)

        # First Res Block
        self.resnet_layer1 = SwiftLaneResBlock.make_layers(64, 64, 2, 1)

        # Second Res Block
        self.resnet_layer2 = SwiftLaneResBlock.make_layers(64, 128, 2, 2)

        # Third Res Block
        self.resnet_layer3 = SwiftLaneResBlock.make_layers(128, 256, 2, 2)

        # SwiftLane: Pooling & Conv
        self.swift_maxpool1 = nn.MaxPool2d(2)
        self.swift_conv1 = nn.Conv2d(256, 8, (1, 1))

        # Estimator
        wp = torch.ones((1, 3, height, width))
        self.feature_shape = self.conv_forwarding(wp).shape

        # SwiftLane: Fully Connected
        self.swift_fl = nn.Flatten()
        self.swift_drop1 = nn.Dropout(0.2)
        self.swift_fc2 = nn.Linear(self.feature_shape[1] * self.feature_shape[2] * self.feature_shape[3], 2048)
        self.swift_drop2 = nn.Dropout(0.2)
        self.swift_out = nn.Linear(2048, c * h * (w + 1))

        # Activation
        self.swift_actv = nn.Softmax(dim=-1)

    def conv_forwarding(self, x: torch.Tensor):
        # ResNet: Initial Conv
        x = self.resnet_conv1(x)
        x = self.resnet_bn1(x)
        x = self.resnet_maxpool1(x)
        # ResNet: Block 1
        x = self.resnet_layer1(x)
        # ResNet: Block 2
        x = self.resnet_layer2(x)
        # ResNet: Block 3
        x = self.resnet_layer3(x)
        # SwiftLane: Conv
        x = self.swift_maxpool1(x)
        x = self.swift_conv1(x)
        return x

    def forward(self, x: torch.Tensor):
        x = self.conv_forwarding(x)
        x = self.swift_fl(x)
        x = self.swift_drop1(x)
        x = self.swift_fc2(x)
        x = self.swift_drop2(x)
        x = self.swift_out(x)
        x = torch.reshape(x, (x.shape[0], self.ch, self.hl, self.wd + 1))
        x = self.swift_actv(x)
        return x


if __name__ == "__main__":
    model = SwiftLaneCNN(3, 288, 800, 1, 1, 1)
    w = torch.ones((1, 3, 288, 800))
    w = model(w)
    print(w.shape)
