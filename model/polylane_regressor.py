import torch
import torch.nn as nn

from model.blocks.swift_resnet import ArvnResNetHelper


class PolyLaneRegressor(nn.Module):
    def __init__(self,
                 height,
                 width,
                 lanes,
                 backbone='resnet50_o',
                 degree=4,
                 in_channels=3,):
        super(PolyLaneRegressor, self).__init__()
        if backbone.startswith('resnet'):
            self.backbone = ArvnResNetHelper.get_arch(arch=backbone,in_channels=in_channels)
        else:
            raise Exception("Backbone not supported")
        if backbone.endswith("o"):
            self.avgpool = nn.Identity()
        else:
            self.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        wp = torch.ones((1, 3, height, width))
        self.feature_shape = self.avgpool(self.backbone(wp)).shape

        # Fc Layers
        self.flatten = nn.Flatten()
        dims = 1
        for i in range(len(self.feature_shape)):
            dims *= self.feature_shape[i]
        self.fc1 = nn.Linear(dims,1000)
        if backbone.endswith("o"):
            self.backbone.fc = nn.Linear(self.backbone.fc.in_features, 1000)
            self.fc1 = nn.Identity()
        self.dropout1 = nn.Dropout(0.2)
        self.fc2 = nn.Linear(1000,256)
        self.dropout2 = nn.Dropout(0.2)
        self.relu = nn.ReLU()

        # Exist Branch
        self.exist_pr = nn.Linear(256,lanes*2) # Softmax is included in the Loss function

        # Regressor Branch
        self.fcr1 = nn.Linear(256,64)
        self.fcr2 = nn.Linear(64,lanes*degree+lanes+1)


    def forward(self,x):
        x = self.backbone(x)
        x = self.avgpool(x)
        x = self.flatten(x)
        x = self.fc1(x)
        x = self.dropout1(x)
        x = self.fc2(x)
        x = self.dropout2(x)
        x_pr = self.exist_pr(x)
        x_re = self.fcr1(x)
        x_re = self.fcr2(x_re)
        x = torch.cat((x_pr,x_re),dim=-1)
        return x