import torch
import torchsummary
import segmentation_models_pytorch as smp
import torchvision
from torch import nn
import torch.nn.functional as torch_fun

from model.alwen_mk2 import AlwenNetMK2GeneralDecoder, AlwenNetMK2OutputIntegrator
from model.alwen_mk2b import AlwenNetMK2AnchorDecoderRectified

from utils.augmentation import *

class AlwenNetMK2Conv(nn.Module):
    """
    Conv-BN-ReLU layer implemented by cfzd
    https://github.com/cfzd/Ultra-Fast-Lane-Detection/blob/master/model/model.py
    """

    def __init__(self, in_channels, out_channels, kernel_size, stride=1, padding=0, dilation=1, bias=False):
        super(AlwenNetMK2Conv, self).__init__()
        self.conv = torch.nn.Conv2d(in_channels, out_channels, kernel_size,
                                    stride=stride, padding=padding, dilation=dilation, bias=bias)
        self.bn = torch.nn.BatchNorm2d(out_channels)
        self.relu = torch.nn.ReLU()

    def forward(self, x):
        x = self.conv(x)
        x = self.bn(x)
        x = self.relu(x)
        return x


class AlwenNetMK2LaneSegBranch(nn.Module):
    """
    Lane Seg Aux Layer implemented by cfzd
    https://github.com/cfzd/Ultra-Fast-Lane-Detection/blob/master/model/model.py
    """

    def __init__(self, c):
        super(AlwenNetMK2LaneSegBranch, self).__init__()
        self.aux_header2 = torch.nn.Sequential(
            AlwenNetMK2Conv(128, 128, kernel_size=3, stride=1, padding=1),
            AlwenNetMK2Conv(128, 128, 3, padding=1),
            AlwenNetMK2Conv(128, 128, 3, padding=1),
            AlwenNetMK2Conv(128, 128, 3, padding=1),
        )
        self.aux_header3 = torch.nn.Sequential(
            AlwenNetMK2Conv(256, 128, kernel_size=3, stride=1, padding=1),
            AlwenNetMK2Conv(128, 128, 3, padding=1),
            AlwenNetMK2Conv(128, 128, 3, padding=1),
        )
        self.aux_header4 = torch.nn.Sequential(
            AlwenNetMK2Conv(512, 128, kernel_size=3, stride=1, padding=1),
            AlwenNetMK2Conv(128, 128, 3, padding=1),
        )
        self.aux_combine = torch.nn.Sequential(
            AlwenNetMK2Conv(384, 256, 3, padding=2, dilation=2),
            AlwenNetMK2Conv(256, 128, 3, padding=2, dilation=2),
            AlwenNetMK2Conv(128, 128, 3, padding=2, dilation=2),
            AlwenNetMK2Conv(128, 128, 3, padding=4, dilation=4),
            torch.nn.Conv2d(128, c + 1, (1, 1))
        )
        initialize_weights(self.aux_header2,self.aux_header3,self.aux_header4,self.aux_combine)

    def forward(self, x4, x3, x2):
        x4 = self.aux_header4(x4)
        x4 = torch_fun.interpolate(x4, scale_factor=4, mode="bilinear")
        x3 = self.aux_header3(x3)
        x3 = torch_fun.interpolate(x3, scale_factor=2, mode="bilinear")
        x2 = self.aux_header2(x2)
        aux_seg = torch.cat([x2, x3, x4], dim=1)
        aux_seg = self.aux_combine(aux_seg)
        return aux_seg


class AlwenNetMK2Gamma(nn.Module):
    def __init__(self,
                 encoder_arch: str = "resnet18",
                 ih: int = 480,
                 iw: int = 800,
                 c: int = 4,
                 h: int = 18,
                 w: int = 100,
                 drop_lsg: bool = False,
                 skip_arrow: bool = False,
                 ):
        super(AlwenNetMK2Gamma, self).__init__()
        # Shared Encoder
        self.skip_arrow = skip_arrow
        self.decoder_channels = [256, 128, 64, 32, 16]
        self.encoder = smp.Unet(encoder_arch, classes=2, encoder_weights="imagenet",
                                decoder_channels=self.decoder_channels).encoder
        # self.replace_batchnorm(self.encoder)
        self.encoder_oc = self.encoder.out_channels
        # Specific Decoder
        self.decoder_arr = AlwenNetMK2GeneralDecoder(self.encoder_oc, self.decoder_channels)
        self.decoder_lsg = AlwenNetMK2LaneSegBranch(c)
        self.decoder_anc = AlwenNetMK2AnchorDecoderRectified(self.encoder_oc[-1], (ih, iw), 32, c, h, w, 8)
        # Activations
        self.actv = AlwenNetMK2OutputIntegrator()
        self.drop_lsg = drop_lsg
        if self.drop_lsg:
            pass
            #self.encoder.set_swish(memory_efficient=False)
        self.arrow_placeholder = torch.randn((ih,iw,3),requires_grad=True)
        initialize_weights(self.decoder_anc)

    def forward(self, fx):
        x = self.encoder(fx)
        if not self.drop_lsg:
            x_anc = self.decoder_anc(x[-1])
            x_lsg = self.decoder_lsg(x[-1],x[-2],x[-3])
            if not self.skip_arrow:
                x_arr = self.decoder_arr(*x)
            else:
                x_arr = fx
            x = self.actv(x_anc, x_arr, x_lsg)
        else:
            x_anc = self.decoder_anc(x[-1])
            x_arr = self.decoder_arr(*x)
            x = self.actv(x_anc, x_arr, x_anc)
        return x


# From https://github.com/cfzd/Ultra-Fast-Lane-Detection/blob/master/model/model.py
def initialize_weights(*models):
    for model in models:
        real_init_weights(model)

def real_init_weights(m):
    if isinstance(m, list):
        for mini_m in m:
            real_init_weights(mini_m)
    else:
        if isinstance(m, torch.nn.Conv2d):
            torch.nn.init.kaiming_normal_(m.weight, nonlinearity='relu')
            if m.bias is not None:
                torch.nn.init.constant_(m.bias, 0)
        elif isinstance(m, torch.nn.Linear):
            m.weight.data.normal_(0.0, std=0.01)
        elif isinstance(m, torch.nn.BatchNorm2d):
            torch.nn.init.constant_(m.weight, 1)
            torch.nn.init.constant_(m.bias, 0)
        elif isinstance(m,torch.nn.Module):
            for mini_m in m.children():
                real_init_weights(mini_m)
        else:
            print('unkonwn module', m)



if __name__ == "__main__":
    model = AlwenNetMK2Gamma()
    # model_2 = torchvision.models.resnet18(pretrained=False)
    torchsummary.summary(model, (3, 480, 800), device="cpu")
