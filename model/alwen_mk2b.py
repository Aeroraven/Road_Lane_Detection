from typing import Union, Tuple

import segmentation_models_pytorch as smp
import torch
import torchsummary
from torch import nn

from model.alwen_mk2 import AlwenNetMK2OutputIntegrator


class AlwenNetMK2GeneralDecoderBlockGN(nn.Module):
    def __init__(self, in_channels, concat_channels, out_channels):
        super(AlwenNetMK2GeneralDecoderBlockGN, self).__init__()
        self.up = nn.UpsamplingNearest2d(scale_factor=2)
        self.bn = nn.BatchNorm2d(out_channels)
        self.conv_relu = nn.Sequential(
            nn.Conv2d(concat_channels + in_channels, out_channels, kernel_size=(3, 3), padding=1, bias=False),
            nn.GroupNorm(16, out_channels),
            nn.ReLU(inplace=True),
            nn.Conv2d(out_channels, out_channels, kernel_size=(3, 3), padding=1, bias=False),
            nn.GroupNorm(16, out_channels),
            nn.ReLU(inplace=True)
        )

    def forward(self, x1, x2):
        x1 = self.up(x1)
        x1 = torch.cat((x1, x2), dim=1)
        x1 = self.conv_relu(x1)
        return x1


class AlwenNetMK2GeneralDecoderGN(nn.Module):
    def __init__(self,
                 in_channels,
                 dec_channels = (256, 128, 64, 32, 16),
                 output_classes: int = 2):
        super(AlwenNetMK2GeneralDecoderGN, self).__init__()
        self.ic = in_channels
        self.decoder_seq = []
        self.dec_chs = dec_channels
        blk = AlwenNetMK2GeneralDecoderBlockGN(in_channels[len(in_channels) - 1],
                                               in_channels[len(in_channels) - 2],
                                               dec_channels[0])
        self.decoder_seq.append(blk)
        for i in range(1, len(self.dec_chs)):
            blk = AlwenNetMK2GeneralDecoderBlockGN(dec_channels[i - 1],
                                                   in_channels[len(in_channels) - i - 2], dec_channels[i])
            self.decoder_seq.append(blk)
        self.blocks = nn.ModuleList(self.decoder_seq)
        self.output_block = nn.Conv2d(dec_channels[-1], output_classes, kernel_size=(3, 3), padding=1)

    def forward(self, *args):
        x = None
        for i, blocks in enumerate(self.blocks):
            if i == 0:
                x = blocks(args[len(self.dec_chs)], args[len(self.dec_chs) - 1])
            else:
                x = blocks(x, args[len(self.dec_chs) - 1 - i])
        x = self.output_block(x)
        return x


class AlwenNetMK2AnchorDecoderRectified(nn.Module):
    def __init__(self,
                 encoder_channels: int,
                 encoder_input,
                 encoder_downscale: int = 32,
                 c: int = 4,
                 h: int = 50,
                 w: int = 100,
                 pool_op: int = 8):
        super(AlwenNetMK2AnchorDecoderRectified, self).__init__()
        self.ec = encoder_channels
        self.c = c
        self.h = h
        self.w = w
        self.pooling = nn.Conv2d(self.ec, pool_op, kernel_size=(1, 1))
        self.fl = nn.Flatten()
        # self.drop = nn.Dropout(0.1)
        self.fw = pool_op * encoder_input[0] * encoder_input[1] // (encoder_downscale ** 2)
        self.fc = nn.Linear(self.fw, 2048)
        self.nl = nn.ReLU()
        self.output = nn.Linear(2048, c * h * (w + 1))

    def forward(self, x):
        x = self.pooling(x)
        x = self.fl(x)
        # x = self.drop(x)
        x = self.fc(x)
        x = self.nl(x)
        x = self.output(x)
        x = torch.reshape(x, (x.shape[0], self.w + 1, self.c, self.h))
        return x


class AlwenNetMK2Beta(nn.Module):
    def __init__(self,
                 encoder_arch: str = "efficientnet-b4",
                 ih: int = 480,
                 iw: int = 800,
                 c: int = 4,
                 h: int = 18,
                 w: int = 100,
                 drop_lsg: bool = False
                 ):
        super(AlwenNetMK2Beta, self).__init__()
        # Shared Encoder
        self.decoder_channels = [256, 128, 64, 32, 16]
        self.encoder = smp.Unet(encoder_arch, classes=2, encoder_weights=None,
                                decoder_channels=self.decoder_channels).encoder
        self.replace_batchnorm(self.encoder)
        self.encoder_oc = self.encoder.out_channels
        # Specific Decoder
        self.decoder_arr = AlwenNetMK2GeneralDecoderGN(self.encoder_oc,self.decoder_channels)
        self.decoder_lsg = AlwenNetMK2GeneralDecoderGN(self.encoder_oc, output_classes=c + 1)
        self.decoder_anc = AlwenNetMK2AnchorDecoderRectified(self.encoder_oc[-1], (ih, iw), 32, c, h, w, 8)
        # Activations
        self.actv = AlwenNetMK2OutputIntegrator()
        self.drop_lsg = drop_lsg
        if self.drop_lsg:
            self.encoder.set_swish(memory_efficient=False)

    def forward(self, x):
        x = self.encoder(x)
        if not self.drop_lsg:
            x_anc = self.decoder_anc(x[-1])
            x_lsg = self.decoder_lsg(*x)
            x_arr = self.decoder_arr(*x)
            x = self.actv(x_anc, x_arr, x_lsg)
        else:
            x_anc = self.decoder_anc(x[-1])
            x_arr = self.decoder_arr(*x)
            x = self.actv(x_anc, x_arr, x_anc)
        return x

    def replace_batchnorm(self, module: torch.nn.Module):
        for name, child in module.named_children():
            if isinstance(child, torch.nn.BatchNorm2d):
                child: torch.nn.BatchNorm2d = child
                setattr(module, name, torch.nn.GroupNorm(8, child.num_features))
            else:
                self.replace_batchnorm(child)


if __name__ == "__main__":
    model = AlwenNetMK2Beta()
    torchsummary.summary(model, (3, 480, 800), device="cpu")
