import tqdm

from datasets.segmentation import SegmentationDataset
from utils.system import fcpy
from utils.utility import *


def process(**kwargs):
    print("Application starts")
    # Hyper Parameters & Environment Settings
    fargs = {}
    for key in kwargs.keys():
        fargs[key] = kwargs[key]

    # Seeds
    set_seed(fargs['random_seed'])

    # Model Configurations
    mask_ext = '.png'
    preproc = smp.encoders.get_preprocessing_fn(fargs["encoder_arch"], fargs["pretrained_encoder_weight"])
    if kwargs['dataset_type'] == 'tu_simple_trapezoid_seg':
        train_dataset = SegmentationDataset(fargs["source_image_path"], fargs["source_mask_path"], [0, 1],
                                            image_size_w=fargs["image_scale_w"], image_size_h=fargs['image_scale_h'],
                                            preprocessing=get_preprocessing(preproc), numpy_dataset=False,
                                            tu_simple_trapezoid_filter=True)
        mask_ext = '.jpg'
    else:
        train_dataset = SegmentationDataset(fargs["source_image_path"], fargs["source_mask_path"], [0, 1],
                                            image_size_w=fargs["image_scale_w"], image_size_h=fargs['image_scale_h'],
                                            preprocessing=get_preprocessing(preproc), numpy_dataset=False)
    # Start Training
    if fargs["output_numpy_file"]:
        train_dataset.save_to_np_file(fargs["output_image_path"], fargs["output_mask_path"])
    else:
        train_dataset.save_to_img_file(fargs["output_image_path"], fargs["output_mask_path"], mask_ext=mask_ext)


def dataset_partition(**kwargs):
    fargs = {}
    for key in kwargs.keys():
        fargs[key] = kwargs[key]
    set_seed(fargs['random_seed'])
    input_path = fargs["output_image_path"]
    train_output_path = fargs["train_image_path"]
    test_output_path = fargs["test_image_path"]
    train_ratio = fargs["train_test_ratio"]
    files = os.listdir(input_path)
    random.shuffle(files)
    train_files = files[:int(len(files) * train_ratio)]
    test_files = files[int(len(files) * train_ratio):]

    for i in tqdm(range(len(train_files)), desc="Creating train dataset", file=sys.stdout, ascii=True):
        fcpy(input_path + "/" + train_files[i], train_output_path + "/" + train_files[i])
    for i in tqdm(range(len(test_files)), desc="Creating test dataset", file=sys.stdout, ascii=True):
        fcpy(input_path + "/" + test_files[i], test_output_path + "/" + test_files[i])


def trapezoid_preprocess(**kwargs):
    fargs = kwargs
    image_path_list = []
    image_path = kwargs["trapezoid_path"]
    dest_path = kwargs["trapezoid_output_path"]
    image_path_walk = os.walk(image_path)
    temp = len(image_path)
    for i, j, k in image_path_walk:
        for r in k:
            if r.endswith(".png") or r.endswith(".npy"):
                image_path_list.append(i[temp + 1:] + "\\" + r)
    for i in tqdm(range(len(image_path_list)), file=sys.stdout, desc="Processing Trapezoid Masks", ascii=True):
        fcpy(image_path + '\\' + image_path_list[i],
             dest_path + "/" + image_path_list[i][:-4].replace("/", "_").replace("\\", "_") + ".jpg",
             remove=False)


if __name__ == "__main__":
    mode = 0  # 1-Trapezoid Processing
    if mode == 0:
        process(**get_config_json())
        # dataset_partition(**get_config_json())
    elif mode == 1:
        trapezoid_preprocess(**get_config_json())
    else:
        raise Exception("Unsupported operation")
