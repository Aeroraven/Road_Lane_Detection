import time

import cv2
import matplotlib.pyplot as plt
import numpy as np
import onnxruntime
import scipy
import torch
import torchmetrics
import tqdm

from datasets.apollo import ApolloDataset
from datasets.culane_mk4 import CULaneDatasetMK4
from utils.preprocessing import preset_processing
import segmentation_models_pytorch as smp


def apollo_test_alwenmk2():
    session = onnxruntime.InferenceSession(r"C:\\Users\\huang\\Desktop\\best.onnx",
                                           providers=['CUDAExecutionProvider',
                                                      'CPUExecutionProvider'])
    print("Model loaded")
    dataset = ApolloDataset(480, 800,
                            preprocessing=preset_processing("imagenet"),
                            is_test=True,
                            arrow_path=r"D:\image\image",
                            arrow_mask_path=r"D:\a_label\a_label")
    iou_meter = torchmetrics.JaccardIndex(2)
    dice_meter = torchmetrics.Dice(num_classes=2, average='macro')
    recall_meter = torchmetrics.Recall(2, average='macro', mdmc_average="global")
    precision_meter = torchmetrics.Precision(2, average='macro', mdmc_average="global")
    f_meter = torchmetrics.F1Score(2, average='macro', mdmc_average="global")
    iou_score = 0
    dice_score = 0
    recall_score = 0
    precision_score = 0
    time_score = 0
    fscore = 0
    cnt = 0
    with tqdm.tqdm(total=len(dataset), desc="Apollo Testing") as t:
        for image, mask in dataset:
            image = image.reshape(1, 3, 480, 800)
            s = time.time()
            result = session.run([], {'input': image})
            time_score += time.time() - s
            seg_pred = result[1][0]
            seg_pred = np.argmax(seg_pred, axis=0)
            seg_pred = np.expand_dims(seg_pred, axis=0)
            seg_pred = torch.from_numpy(seg_pred)
            mask = np.expand_dims(mask, axis=0)
            mask = torch.from_numpy(mask)
            iou = iou_meter(seg_pred, mask)
            recall = recall_meter(seg_pred, mask)
            precision = precision_meter(seg_pred, mask)
            f1 = f_meter(seg_pred, mask)
            dice = dice_meter(seg_pred, mask)
            iou_score += iou.numpy()
            recall_score += recall.numpy()
            precision_score += precision.numpy()
            fscore += f1.numpy()
            dice_score += dice.numpy()
            cnt += 1
            t.update(1)
            t.set_postfix(iou=iou_score / cnt, recall=recall_score / cnt, precision=precision_score / cnt,
                          f1=fscore / cnt,
                          dice=dice_score / cnt, dice_c=dice.numpy(), time=time_score / cnt)


def ultra_visualize(img, result, griding_num=200, img_w=800, img_h=480):
    col_sample = np.linspace(0, 800 - 1, griding_num)
    col_sample_w = col_sample[1] - col_sample[0]
    cls_num_per_lane = 18
    row_anchor_w = [121, 131, 141, 150, 160, 170, 180, 189, 199, 209, 219, 228, 238, 248, 258, 267, 277, 287]
    row_anchor = [int(1.0 * row_anchor_w[i] / 288.0 * 480) for i in range(len(row_anchor_w))]
    out_j = result.data.cpu().numpy()
    out_j = out_j[0]

    out_j_2 = np.argmax(out_j, axis=0)
    out_j_3 = np.transpose(out_j, (1, 2, 0))
    out_j = out_j[:, ::-1, :]
    prob = scipy.special.softmax(out_j[:-1, :, :], axis=0)
    idx = np.arange(griding_num) + 1
    idx = idx.reshape(-1, 1, 1)
    loc = np.sum(prob * idx, axis=0)
    out_j = np.argmax(out_j, axis=0)
    loc[out_j == griding_num] = 0
    out_j = loc

    ls = ""
    for i in range(out_j.shape[1]):
        if np.sum(out_j[:, i] != 0) > 2:
            for k in range(out_j.shape[0]):
                if out_j[k, i] > 0:
                    dx = int(out_j[k, i] * col_sample_w * img_w / 800) - 1
                    dx = dx / 800 * 1640
                    dy = int(img_h * (row_anchor[cls_num_per_lane - 1 - k] / 480)) - 1
                    dy = dy / 480 * 590
                    ls += str(int(dx)) + " " + str(int(dy)) + " "
            ls += "\n"
    return img, ls


def ultra_test():
    print("Starts")
    dataset = CULaneDatasetMK4(r"D:\driver_37_30frame",
                               "", ['driver_37_30frame'],
                               4, 200, 18, 30, 10, 480, 800, 8,
                               preprocessing=preset_processing("imagenet"),
                               train_split=0,
                               test_only=True)
    print("Dataset Ready")
    session = onnxruntime.InferenceSession(r"C:\\Users\\huang\\Desktop\\best.onnx",
                                           providers=['CUDAExecutionProvider',
                                                      'CPUExecutionProvider'])
    print("Model loaded")
    start = time.time()
    cnt = 0
    with tqdm.tqdm(total=len(dataset), desc="Exporting Lane Anchors") as t:
        fw = len(dataset)
        for image, path in dataset:
            cnt += 1
            path = path.replace(r"D:\driver_37_30frame", r"D:\test")
            image_n = np.expand_dims(image, axis=0)
            result = session.run([], {'input': image_n})
            result = result[0]
            result = np.transpose(result, (0, 1, 3, 2))
            result = torch.from_numpy(np.array(result))
            vis, ls = ultra_visualize(image, result)
            with open(path.replace('.jpg', '.lines.txt'), "w") as f:
                f.write(ls)
            tm = (fw-cnt)*(time.time()-start)/cnt
            m, s = divmod(tm, 60)
            h, m = divmod(m, 60)
            t.set_postfix(remaining=str(int(h))+":"+str(int(m))+":"+str(int(s)),cur=path)
            t.update(1)


if __name__ == "__main__":
    ultra_test()
