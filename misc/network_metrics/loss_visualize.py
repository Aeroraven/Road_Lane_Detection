import json
import math
import re

import matplotlib.pyplot as plt


def repl(matched):
    value = matched.group('value')
    return '"' + str(value[:-1]) + '":'


def merge(dict1, dict2):
    res = {**dict1, **dict2}
    return res


if __name__ == "__main__":
    plt.style.use('ggplot')
    kw = "lane_acc"
    test_file_list = [
        # r"E:\test_result.json",
        # r"C:\Users\huang\Desktop\w.JSON",
        r"C:\即时通讯文件\1531052461\FileRecv\test_result (4).json",
    ]
    train_file_list = [

        # r"E:\train_result.json",
        #r"C:\Users\huang\Desktop\k.JSON",
        r"C:\即时通讯文件\1531052461\FileRecv\train_result (4).json",
    ]
    dc = dict()
    for i in train_file_list:
        with open(i, "r") as f:
            st = f.read()
            # st = st.replace("'", "\"")
            # st = re.sub('(?P<value>[0-9]+:)', repl, st)
            print(st)
            dc = merge(json.loads(st), dc)
    ky = dc.keys()
    ky = [int(i) for i in ky]
    mx = max(ky)
    dx = []
    dy = []
    ldy = []
    for i in range(mx):
        dxt = i
        dyt = dc[str(i)][kw]
        dx.append(dxt)
        dy.append(dyt)
        ldy.append(dyt)
    plt.subplot(1, 2, 1)
    plt.yscale("log")
    plt.plot(dx, dy, label="Train")

    plt.subplot(1, 2, 2)
    plt.yscale("linear")
    plt.plot(dx, ldy, label="Train")


    dc = dict()
    for i in test_file_list:
        with open(i, "r") as f:
            st = f.read()
            # st = st.replace("'", "\"")
            # st = re.sub('(?P<value>[0-9]+:)', repl, st)
            print(st)
            dc = merge(json.loads(st), dc)
    ky = dc.keys()
    ky = [int(i) for i in ky]
    mx = max(ky)
    dx = []
    dy = []
    ldy = []
    for i in range(mx):
        dxt = i
        dyt = dc[str(i)][kw]
        dx.append(dxt)
        dy.append(dyt)
        ldy.append(dyt)


    plt.subplot(1, 2, 1)
    plt.yscale("log")
    plt.plot(dx, dy, label="Test/Validation")
    plt.legend()
    plt.title("Logarithmic Metric (Key:" + kw + ")")
    plt.subplot(1, 2, 2)
    plt.yscale("linear")
    plt.plot(dx, ldy, label="Test/Validation")
    plt.legend()
    plt.title("Metric (Key:" + kw + ")")
    plt.show()
