import os
import random
import sys

import cv2
import numpy as np
import tqdm

def return_processed_arrows(image: np.ndarray):
    output = np.zeros((image.shape[0], image.shape[1]))
    ARROW_COLOR_LIST = [
        [255,128,0],
        [255,0,0],
        [255,0,255],
        [255,0,127],
        [255,102,178]
    ]
    for color in ARROW_COLOR_LIST:
        image_channel_transposed = np.transpose(image, (2, 0, 1))
        image_thresh_r = np.zeros((image.shape[0], image.shape[1]))
        image_thresh_g = np.zeros((image.shape[0], image.shape[1]))
        image_thresh_b = np.zeros((image.shape[0], image.shape[1]))
        image_thresh_r[image_channel_transposed[2] == color[0]] = 1
        image_thresh_g[image_channel_transposed[1] == color[1]] = 1
        image_thresh_b[image_channel_transposed[0] == color[2]] = 1
        image_thresh_filtered = image_thresh_r * image_thresh_b * image_thresh_g
        output += image_thresh_filtered
    output = np.minimum(output, 1)
    return output.astype("int64")


if __name__ == "__main__":
    random.seed(3407)
    ip = r"E:\CeyMo\train\mask_annotations"
    ir = r"E:\CeyMo\train\mask_annotations"
    ops = r"E:\ssr\image"
    opr = r"E:\ssr\label"
    fw = os.walk(ip)
    flst = []
    print("Indexing Folder...")
    for i, j, k in fw:
        for r in k:
            flst.append((i.replace(ip, ""), r, r))
    random.shuffle(flst)

    print("Copying & Down-sampling Images...")
    valid = 0
    sup = 0
    discarded = 0
    corrupted = 0
    with tqdm.tqdm(total=len(flst), file=sys.stdout) as f:
        for i in range(len(flst)):
            mask = cv2.imread(ir + flst[i][0] + "\\" + flst[i][2])
            mask = cv2.cvtColor(mask,cv2.COLOR_BGR2RGB)
            try:
                mask = cv2.cvtColor(mask, cv2.COLOR_BGR2RGB)
                mask = cv2.resize(mask, (800, 480), interpolation=cv2.INTER_NEAREST)
                mask = return_processed_arrows(mask)
                fw = np.sum(mask)

                if fw >= 200:
                    valid += 1
                    f.set_postfix(valid=valid, suppressed=sup, discarded=discarded, corrupted=corrupted)
                    f.update(1)
                elif fw >= 1:
                    sup += 1
                    f.set_postfix(valid=valid, suppressed=sup, discarded=discarded, corrupted=corrupted)
                    f.update(1)
                    continue
                else:
                    discarded += 1
                    f.set_postfix(valid=valid, suppressed=sup, discarded=discarded, corrupted=corrupted)
                    f.update(1)
                    continue
            except:
                corrupted += 1
                f.set_postfix(valid=valid, suppressed=sup, discarded=discarded, corrupted=corrupted)
                f.update(1)
                continue