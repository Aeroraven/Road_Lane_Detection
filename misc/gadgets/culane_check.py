import os
import os.path
import sys
from typing import Tuple

import cv2
import jpeg4py
import numpy as np
import torch.utils.data
import albumentations as albu
from tqdm import tqdm

from utils.augmentation import get_tu_simple_augmentation
from utils.system import file_readlines, fcpy


class CULaneDatasetMK2(torch.utils.data.Dataset):
    def __init__(self,
                 image_path: str,
                 seg_mask_path: str,
                 sub_directories = None,
                 c: int = 4,
                 w: int = 170,
                 h: int = 59,
                 h_min: int = 0,
                 wm: int = 10,
                 ih: int = 400,
                 iw: int = 800,
                 augmentation: callable = get_tu_simple_augmentation,
                 preprocessing: callable = None,
                 sanity_check: bool = True):
        super(CULaneDatasetMK2, self).__init__()
        if sub_directories is None:
            sub_directories = ['driver_161_90frame']
        self.files = []
        self.mask_idx = {}
        self.line_idx = {}
        self.c = c
        self.w = w
        self.h = h
        self.hm = h_min
        self.wm = wm
        self.ih = ih
        self.iw = iw
        self.preprocessing = preprocessing
        self.augmentation = augmentation
        for sd in sub_directories:
            lst = list(os.walk(image_path))
            valid = 0
            corrupted = 0
            anchor_corrupted = 0
            mask_corrupted = 0
            with tqdm(total=len(lst), desc="Sanity Check", ascii=True, file=sys.stdout) as f:
                for i, j, k in lst:
                    for r in k:
                        if r.endswith(".jpg"):
                            ip = i.replace(image_path, "")
                            self.line_idx[i + "\\" + r] = np.array([0])
                            if os.path.exists(seg_mask_path + "\\" + ip + "\\" + r[:-4] + ".png"):

                                self.mask_idx[i + "\\" + r] = seg_mask_path + "\\" + ip + "\\" + r[:-4] + ".png"

                                if not sanity_check:
                                    self.files.append(i + "\\" + r)
                                    valid += 1
                                else:
                                    image, (mask, anchor) = self.__getitem_impl__(i + "\\" + r, True)
                                    fr = np.min(anchor)
                                    fw = np.max(mask)
                                    if (fr == self.w and fw != 0) or (fw == 0 and fr != self.w):
                                        corrupted += 1
                                    elif fr == self.w:
                                        anchor_corrupted += 1
                                        self.files.append(i + "\\" + r)
                                        fcpy(i + "\\" + r, r"E:\Nf\culanew\\" + (ip + "\\" + r).replace("\\", "%"),
                                             remove=False)
                                        fcpy(i + "\\" + r[:-4] + ".lines.txt",
                                             r"E:\Nf\culanew\\" + (ip + "\\" + r[:-4] + ".lines.txt").replace("\\",
                                                                                                              "%"),
                                             remove=False)

                                    elif fw == 0:
                                        mask_corrupted += 1
                                    else:
                                        self.files.append(i + "\\" + r)
                                        valid += 1
                                        fcpy(i + "\\" + r, r"E:\Nf\culanew\\" + (ip + "\\" + r).replace("\\", "%"),
                                             remove=False)
                                        fcpy(i + "\\" + r[:-4] + ".lines.txt",
                                             r"E:\Nf\culanew\\" + (ip + "\\" + r[:-4] + ".lines.txt").replace("\\",
                                                                                                              "%"),
                                             remove=False)

                    f.set_postfix(valid=valid,empty_lane=anchor_corrupted,mask_corrupted=mask_corrupted,
                                  corrupted=corrupted)
                    f.update(1)

    def parse_line(self, file):
        s_map = np.ones((self.c, self.h - self.hm)) * self.w
        fr = file_readlines(file)
        for i in range(len(fr)):
            fr[i] = fr[i].replace('\n', '').rstrip()
            kp = fr[i].split(' ')
            for j in range(0, len(kp), 2):
                xpos = int(float(kp[j])) // self.wm
                ypos = int(kp[j + 1]) // 10 - self.hm - 1
                s_map[i, ypos] = xpos

        return s_map

    def albu_resize(self):
        train_transform = albu.Compose([albu.Resize(self.ih, self.iw, interpolation=cv2.INTER_NEAREST)])
        return train_transform

    def __getitem__(self, item):
        fn = self.files[item]
        return self.__getitem_impl__(fn)

    def __getitem_impl__(self, fn, sanity_test=False) -> Tuple[np.ndarray, Tuple[np.ndarray, np.ndarray]]:
        image = jpeg4py.JPEG(fn).decode()
        mask = cv2.imread(self.mask_idx[fn])
        # print("READ",self.mask_idx[fn], np.max(mask))
        if self.line_idx[fn].shape[0] == 1:
            self.line_idx[fn] = self.parse_line(fn[:-4] + ".lines.txt")
        anchor: np.ndarray = self.line_idx[fn]
        if not sanity_test:
            resize_sample = self.albu_resize()(image=image, mask=mask)
            image = resize_sample['image']
            mask = resize_sample['mask']
        if self.augmentation and not sanity_test:
            sample = self.augmentation()(image=image)
            image = sample['image']
        if self.preprocessing and not sanity_test:
            sample = self.preprocessing()(image=image)
            image = sample['image']
        return image, (mask, anchor)

    def __len__(self):
        return len(self.files)


if __name__ == "__main__":
    dataset = CULaneDatasetMK2(r"E:\新建文件夹 (4)\driver_161_90frame",
                               r"E:\Nf\laneseg_label_w16\laneseg_label_w16")
    for i in range(500):
        sf = dataset[i]
