import cv2
import matplotlib.pyplot as plt
import numpy as np
import tqdm

from datasets.culane_mk4 import CULaneDatasetMK4

if __name__ == "__main__":
    dataset = CULaneDatasetMK4(image_path=r"C:\Users\huang\Pictures\sm\a",
                               seg_mask_path=r"C:\Users\huang\Pictures\sm\b",
                               is_test=False,
                               sanity_check=False,
                               train_split=1)
    print(len(dataset))
    for i in tqdm.trange(500):
        image, (mask, anchor) = dataset[0]
