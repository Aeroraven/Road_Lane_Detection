import os
import random
import sys

import cv2
import jpeg4py
import numpy as np
import tqdm
from PIL import Image
from matplotlib import pyplot as plt

from utils.system import *


def return_processed_arrows(image: np.ndarray):
    output = np.zeros((image.shape[0], image.shape[1]))
    ARROW_COLOR_LIST = [
        [128, 78, 160],
        [150, 100, 100],
        [180, 165, 180],
        [107, 142, 35],
        [128, 128, 0],
        [0, 0, 230]
    ]
    for color in ARROW_COLOR_LIST:
        image_channel_transposed = np.transpose(image, (2, 0, 1))
        image_thresh_r = np.zeros((image.shape[0], image.shape[1]))
        image_thresh_g = np.zeros((image.shape[0], image.shape[1]))
        image_thresh_b = np.zeros((image.shape[0], image.shape[1]))
        image_thresh_r[image_channel_transposed[2] == color[0]] = 1
        image_thresh_g[image_channel_transposed[1] == color[1]] = 1
        image_thresh_b[image_channel_transposed[0] == color[2]] = 1
        image_thresh_filtered = image_thresh_r * image_thresh_b * image_thresh_g
        output += image_thresh_filtered
    output = np.minimum(output, 1)
    return output.astype("int64")


if __name__ == "__main__":
    random.seed(3407)
    ip = r"E:\ColorImage_road02\ColorImage_road02\ColorImage"
    ir = r"E:\新建文件夹\Labels_road02\Labels_road02\Label"
    ops = r"E:\ssr2\image"
    opr = r"E:\ssr2\label"
    fw = os.walk(ip)
    flst = []
    print("Indexing Folder...")
    for i, j, k in fw:
        for r in k:
            flst.append((i.replace(ip, ""), r, r.replace(".jpg", "_bin.png")))
    random.shuffle(flst)

    print("Copying & Down-sampling Images...")
    valid = 0
    sup = 0
    discarded = 0
    corrupted = 0
    with tqdm.tqdm(total=len(flst), file=sys.stdout) as f:
        for i in range(len(flst)):
            mask = cv2.imdecode(np.fromfile(ir + flst[i][0] + "\\" + flst[i][2], dtype=np.uint8), cv2.IMREAD_COLOR)
            try:
                # mask = cv2.cvtColor(mask, cv2.COLOR_BGR2RGB)
                mask = cv2.resize(mask, (600, 360), interpolation=cv2.INTER_NEAREST)
                mask = return_processed_arrows(mask)
                fw = np.sum(mask)

                if fw >= 250:
                    valid += 1
                    f.set_postfix(valid=valid, suppressed=sup, discarded=discarded, corrupted=corrupted)
                    f.update(1)
                elif fw >= 1:
                    sup += 1
                    f.set_postfix(valid=valid, suppressed=sup, discarded=discarded, corrupted=corrupted)
                    f.update(1)
                    continue
                else:
                    discarded += 1
                    f.set_postfix(valid=valid, suppressed=sup, discarded=discarded, corrupted=corrupted)
                    f.update(1)
                    continue
            except:
                corrupted += 1
                f.set_postfix(valid=valid, suppressed=sup, discarded=discarded, corrupted=corrupted)
                f.update(1)
                continue

            fcpy(ip + flst[i][0] + "\\" + flst[i][1], ops + "\\" + flst[i][0].replace("\\", "_") + "_" + flst[i][1],
                 remove=False)
            fcpy(ir + flst[i][0] + "\\" + flst[i][2], opr + "\\" + flst[i][0].replace("\\", "_") + "_" + flst[i][2],
                 remove=False)
            img = cv2.imread(ops + "\\" + flst[i][0].replace("\\", "_") + "_" + flst[i][1])
            imgr = cv2.resize(img, (800, 480), interpolation=cv2.INTER_AREA)
            cv2.imwrite(ops + "\\" + flst[i][0].replace("\\", "_") + "_" + flst[i][1], imgr)
