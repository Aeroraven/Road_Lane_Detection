import random
import torch
from torch.utils.data import DataLoader


class DLSet(torch.utils.data.Dataset):
    def __init__(self) -> None:
        super().__init__()
        random.seed(3407)
    
    def __getitem__(self,idx):
        val = random.random()
        print(id(self),val)
        return val
    
    def __len__(self):
        return 5


if __name__ == "__main__":
    ds = DLSet()
    dr = DataLoader(ds, batch_size = 8, num_workers=4)
    for i in dr:
        print(i)