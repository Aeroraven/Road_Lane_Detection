import os

import cv2
import tqdm

if __name__ == "__main__":
    image_path = r"E:\NF4\driver_182_30frame"
    lst = list(os.walk(image_path))
    for i, j, k in tqdm.tqdm(lst):
        for r in k:
            if r.endswith("jpg"):
                img = cv2.imread(i + "\\" + r)
                imgr = cv2.resize(img, (800, 480), interpolation=cv2.INTER_AREA)
                cv2.imwrite(i + "\\" + r, imgr)
