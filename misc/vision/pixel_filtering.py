import cv2
import matplotlib.pyplot as plt
import numpy as np


def return_processed_arrows(arrow_list,
                            image: np.ndarray):
    output = np.zeros((image.shape[0], image.shape[1]))
    for color in arrow_list:
        image_channel_transposed = np.transpose(image, (2, 0, 1))
        image_thresh_r = np.zeros((image.shape[0], image.shape[1]))
        image_thresh_g = np.zeros((image.shape[0], image.shape[1]))
        image_thresh_b = np.zeros((image.shape[0], image.shape[1]))
        image_thresh_r[image_channel_transposed[0] == color[0]] = 1
        image_thresh_g[image_channel_transposed[1] == color[1]] = 1
        image_thresh_b[image_channel_transposed[2] == color[2]] = 1
        image_thresh_filtered = image_thresh_r * image_thresh_b * image_thresh_g
        output += image_thresh_filtered
    output = np.minimum(output, 1)
    return output


if __name__ == "__main__":
    image = cv2.imread("suzuran.jfif")
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    ARROW_COLOR_LIST = [
        [128, 78, 160],  # 前行/左转
        [150, 100, 100],  # 前行/右转
        [180, 165, 180],  # 左转
        [107, 142, 35],  # 右转
        [128, 128, 0],  # 前行
        [0, 0, 230]  # 掉头
    ]
    output = return_processed_arrows(ARROW_COLOR_LIST, image)

    plt.imshow(output)
    plt.show()
