import cv2
import matplotlib.pyplot as plt
import numpy as np


def otsu_canny_threshold(t, hist, hist_prefix_sum, histexp_prefix_sum):
    a0 = hist_prefix_sum[t]
    a1 = hist_prefix_sum[-1] - hist_prefix_sum[t]
    if a0 == 0 or a1 == 0:
        return 0
    mu0 = histexp_prefix_sum[t] / a0
    mu1 = (histexp_prefix_sum[-1] - histexp_prefix_sum[t]) / a1
    return a0 * a1 * (mu0 - mu1) ** 2


def otsu_canny_hist_stat(hist):
    hist_prefix_sum = []
    hist_prefix_sum_t = 0
    histexp_prefix_sum = []
    histexp_prefix_sum_t = 0
    for i in range(len(hist)):
        histexp_prefix_sum_t += hist[i] * i
        hist_prefix_sum_t += hist[i]
        hist_prefix_sum.append(hist_prefix_sum_t)
        histexp_prefix_sum.append(histexp_prefix_sum_t)
    return hist, hist_prefix_sum, histexp_prefix_sum


def otsu_canny_higher_threshold(hist):
    h, hs, hes = otsu_canny_hist_stat(hist)
    stat_map = []
    val = 0
    best_val = 0
    best_val_idx = 0
    for i in range(0, 256):
        val = otsu_canny_threshold(i, h, hs, hes)
        if val > best_val:
            best_val_idx = i
            best_val = val
        stat_map.append(val)
    return stat_map, best_val_idx, best_val


def dark_channel_prior(image, radius=2):
    r = image[:, :, 0]
    g = image[:, :, 1]
    b = image[:, :, 2]
    r = np.pad(r, ((radius, radius), (radius, radius)), mode="edge")
    g = np.pad(g, ((radius, radius), (radius, radius)), mode="edge")
    b = np.pad(b, ((radius, radius), (radius, radius)), mode="edge")
    min_x = np.minimum(r, g)
    min_x = np.minimum(min_x, b)
    ret = np.zeros((image.shape[0], image.shape[1]))
    print(r.shape)
    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            ret[i, j] = np.min(min_x[i:i + (2 * radius + 1), j:j + (2 * radius + 1)])
    return ret


def grayscale_ups(image):
    return np.concatenate((image, image, image), axis=-1)


subplot_id = 0


def new_subplot():
    cols = 3
    rows = 3
    global subplot_id
    subplot_id += 1
    plt.subplot(rows, cols, subplot_id)


if __name__ == "__main__":
    image = cv2.imread(r"road.jpg")
    image = cv2.GaussianBlur(image, (5, 5), 16)
    image_z = np.reshape(image, (-1, 3))
    print(image_z.shape)
    image_g = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    image_g = np.expand_dims(image_g, axis=-1)
    hist = cv2.calcHist(image_g, [0], None, [256], (0, 256), accumulate=False)
    hist = hist.squeeze()
    label = list(range(0, 256))
    otsu_hist, otsu_best, otsu_best_val = otsu_canny_higher_threshold(hist)

    new_subplot()
    plt.title("Input")
    image_x = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    plt.imshow(image_x)

    new_subplot()
    plt.title("Grayscale Image")
    image_g = image_g.squeeze()
    plt.imshow(image_g, cmap="gray")

    new_subplot()
    plt.title("Otsu-Canny Output")
    canny = cv2.Canny(image_g, 0.5*otsu_best, otsu_best)
    plt.imshow(canny, cmap="gray_r")

    new_subplot()
    plt.title("Dark Channel Prior")
    dc_prior = dark_channel_prior(image)
    plt.imshow(dc_prior, cmap="gray")

    new_subplot()
    plt.title("KMeans Clustering(K=8)")
    km_criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
    km_ret, km_label, km_center = cv2.kmeans(np.float32(image_g).flatten(), 8, None, km_criteria, 10,
                                             cv2.KMEANS_RANDOM_CENTERS)
    print(km_center.shape)
    km_vis = np.uint8(km_center)[km_label.flatten()].reshape((image_g.shape[0], image_g.shape[1]))
    plt.imshow(km_vis)

    new_subplot()
    plt.title("KMeans Dark Channel Prior(K=8)")
    km_criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
    km_ret_2, km_label, km_center = cv2.kmeans(np.float32(dc_prior).flatten(), 8, None, km_criteria, 10,
                                               cv2.KMEANS_RANDOM_CENTERS)
    print(km_center.shape)
    km_vis2 = np.uint8(km_center)[km_label.flatten()].reshape((image_g.shape[0], image_g.shape[1]))
    plt.imshow(km_vis2)

    new_subplot()
    plt.title("KMeans Dark Channel Prior Otsu-Canny")
    otsu_hist, otsu_best, _ = otsu_canny_higher_threshold(cv2.calcHist(km_vis2, [0], None, [256], (0, 256), accumulate=False).squeeze())
    canny_2 = cv2.Canny(km_vis2,otsu_best*0.5, otsu_best)
    plt.imshow(canny_2,cmap="gray_r")
    plt.show()
