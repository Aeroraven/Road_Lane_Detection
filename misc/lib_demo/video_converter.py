# Acknowledgement declaration
# Adapted from code: https://www.geeksforgeeks.org/python-create-video-using-multiple-images-using-opencv/
import os
import sys
import cv2
from PIL import Image
from tqdm import tqdm

print(os.getcwd())
os.chdir(r"C:\Users\huang\Desktop\ff\05081544_0305")
path = r"C:\Users\huang\Desktop\ff\05081544_0305"
mean_height = 0
mean_width = 0
num_of_images = len(os.listdir(path))
for file in os.listdir(path):
    im = Image.open(os.path.join(path, file))
    width, height = im.size
    mean_width += width
    mean_height += height

mean_width = int(mean_width / num_of_images)
mean_height = int(mean_height / num_of_images)

s = os.listdir(path)
with tqdm(total=len(s), file=sys.stdout, desc="Resizing") as t:
    for file in s:
        t.update(1)
        if file.endswith(".jpg") or file.endswith(".jpeg") or file.endswith("png"):
            im = Image.open(os.path.join(path, file))

            width, height = im.size

            imResize = im.resize((mean_width, mean_height), Image.ANTIALIAS)
            imResize.save(file, 'JPEG', quality=95)


def generate_video():
    image_folder = '.'
    video_name = 'mygeneratedvideo.avi'
    os.chdir(r"C:\Users\huang\Desktop\ff\05081544_0305")
    images = [img for img in os.listdir(image_folder)
              if img.endswith(".jpg") or
              img.endswith(".jpeg") or
              img.endswith("png")]
    images.sort()

    frame = cv2.imread(os.path.join(image_folder, images[0]))

    height, width, layers = frame.shape
    video = cv2.VideoWriter(video_name, 0, 30, (width, height))

    with tqdm(total=len(images), file=sys.stdout, desc="Converting") as t:
        for image in images:
            video.write(cv2.imread(os.path.join(image_folder, image)))
            t.update(1)

    cv2.destroyAllWindows()
    video.release()


if __name__ == "__main__":
    generate_video()
