from datasets.tusimple import *
from utils.utility import *

if __name__ == "__main__":
    kwargs = get_config_json()
    datasets = TuSimpleDataset(image_path=kwargs['train_image_path'],
                               image_size_h=kwargs['image_scale_h'],
                               image_size_w=kwargs['image_scale_w'],
                               index_subdirectories=kwargs['tu_simple_train_list'],
                               preprocessing=get_preproc_func(**kwargs))